import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from './components/main-page-layout/header';
import Main from "./components/main-page/main";
import Footer from "./components/main-page-layout/footer";
import NewsPage from "./components/news-page/news-page";
import AllProjectsPage from "./components/main-page-layout/projects/all-projects/all-projects-component/archive";
import ProjectDetailsContainer from "./components/main-page-layout/projects/project-details/project-details-container";
import NewsAboutPage from "./components/news-about-page/news-about-page";
import TextPage from "./components/text-page/text-page";
import RaitingPage from "./components/raiting-page/raiting-page";
import UserCabinet from "./components/user-cabinet-page/user-cabinet-page";
import EditTextPage from "./components/EditText/EditText";
import PartnersPage from "./components/partners-page/partners-page";
import NewProjects from "./components/main-page-layout/projects/projects-requests/project-request-page/project-request-page";

function App() {
  return (
      <BrowserRouter>
        <div className="App">
          <Header/>
          <Switch>
              <Route exact path='/' component={Main} />
              <Route path='/cabinet' component={UserCabinet}/>
              <Route path='/raiting' component={RaitingPage}/>
              <Route path='/text-page' component={TextPage}/>
              <Route path='/edit-text-page' component={EditTextPage}/>
              <Route path='/projects' component={AllProjectsPage}/>
              <Route path='/news' component={NewsPage}/>
              <Route path='/about-news' component={NewsAboutPage}/>
              <Route path='/about-project/:id' component={ProjectDetailsContainer}/>
              <Route path='/partners' component={PartnersPage}/>
              <Route path='/project-request' component={NewProjects}/>
          </Switch>
          <Footer/>
        </div>
      </BrowserRouter>
  );
}

export default App;
