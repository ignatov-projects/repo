var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
const creds = require('../../../config/nodeMailerConfig');

var transport = {
    host: 'smtp.gmail.com',
    auth: {
        user: creds.USER,
        pass: creds.PASS
    }
};

var transporter = nodemailer.createTransport(transport);

transporter.verify((error, success) => {
    if (error) {
        console.log(error);
    } else {
        console.log('Server is ready to take messages');
    }
});

router.post('/send', (req, res, next) => {
    var name = req.body.project.author;
    var reason = req.body.project.reason;
    var email = req.body.project.email;
    var content = `Уважаемый(-ая) ${name}! \n Ваш email был оставлен в заявке на прект ${email}. \n Мы отклонили ваш проект. Причина отказа: ${reason} `;

    var mail = {
        to: email,
        subject: 'Ваш проект отклонен.',
        text: content
    };

    transporter.sendMail(mail, (err, data) => {
        if (err) {
            res.json({
                msg: 'fail'
            })
        } else {
            res.json({
                msg: 'success'
            })
        }
    })
});

router.post('/send/subscribe', (req, res, next) => {
    var name = req.body.userData.author;
    var email = req.body.userData.email;
    var content = `Уважаемый(-ая) ${name}! \n Ваш email был оставлен в заявке на прект ${email}. \n Мы рассматриваем ваш проект.`;

    var mail = {
        to: email,
        subject: 'Ваш проект принят на рассмотрение.',
        text: content
    };

    transporter.sendMail(mail, (err, data) => {
        if (err) {
            res.json({
                msg: 'fail'
            })
        } else {
            res.json({
                msg: 'success'
            })
        }
    })
});

module.exports = router;