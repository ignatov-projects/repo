import React from 'react';
import CloseBtn from "../../images/close-btn";

class PaymentSystem extends React.Component {

    render() {

        let re = /\d{1,3}(?=(\d{3})+(?!\d))/g;

    return (
        <section className='payment-system-page-container'>
            <div className="payment-system-page-wrapper">
                <i className='close-btn' onClick={this.props.closeBtn}>
                    <CloseBtn/>
                </i>
                <form onSubmit={this.props.submitPayment}>
                    <div style={{display: 'flex'}}>
                        <h1>Сумма платежа: { this.props.value.replace(re, "$& ") } </h1>
                        <select name="currency" id="currency" className='select' onChange={this.props.handleMoneyChange}>
                            <option value="uah">UAH</option>
                            <option value="usd">USD</option>
                            <option value="rur">RUR</option>
                        </select>
                    </div>
                    <div className="payment-input-value">
                        <input placeholder='Сумма платежа' id='value' type="text" required pattern='^(?!0.*$)([0-9]{1,9}?)$' onChange={this.props.handleMoneyChange} />
                    </div>
                    <div className="payment-system-page-content">
                        <div className="number-cvv-container">
                            <div className="card-number-wrapper">
                                <label htmlFor="card-number">Номер карты</label>
                                <input name='card-cardNumber' id='cardNumber'
                                       required
                                       type="text"
                                       pattern='^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$'
                                       value={this.props.cardNumber}
                                       onChange={this.props.handleMoneyChange}
                                />
                            </div>
                            <div className="card-cvv-wrapper">
                                <label htmlFor="card-cvv-field">CVV2</label>
                                <input name='card-cvv-field' id='cvvNumber' required pattern='^(?!0.*$)([0-9]{3}?)$' type="text"/>
                            </div>
                        </div>

                        <div className="card-name-wrapper">
                            <label htmlFor="card-name">Имя владельца карты</label>
                            <input name='card-name' id='cardName' required type="text"/>
                        </div>
                        <div className="card-date-wrapper">
                            <label htmlFor="card-month-year-field">Срок действия</label>
                            <div className="card-month-year-field">
                                <input name='card-month-field' required type="text"/>
                                <span>/</span>
                                <input type="text"/>
                            </div>
                        </div>
                        <div className="anonimus-container">
                            <label className="anonim">Анонимно
                                <input type="checkbox" name='anonymous' id='anonymous' onChange={this.props.handleAnonymousChange}/>
                                <span className="checkmark"></span>
                            </label>
                        </div>
                        <button type='submit' className='primary-btn'>Оплатить</button>
                    </div>
                </form>
            </div>
        </section>
    )
    }
};

export default PaymentSystem;