import React, {Component} from 'react';
import Join from "../main-page-layout/join";
import Intro from "../main-page-layout/intro";
import Projects from "../main-page-layout/projects/main-page-projects/projects-component/projects";
import Partners from "../main-page-layout/partners";
import Raiting from "../main-page-layout/raiting";
import News from "../main-page-layout/news";

class Main extends Component {
    render() {
        return(
            <div className="main-content">
                <Join/>
                <Intro/>
                <Projects/>
                <Partners/>
                <Raiting/>
                <News/>
            </div>
        )
    }
}

export default Main;