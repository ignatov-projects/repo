import React from 'react';
import Carousel from "./carousel";

const Partners = () => {
    return (
        <section className='partners-container'>
            <h1>Партнеры</h1>
            <div className="partners">
                <Carousel/>
            </div>
        </section>
    )
};
export default Partners;