import React from 'react';
import photo1 from '../../images/1photo.png';
import photo2 from '../../images/2photo.png';
import photo3 from '../../images/3photo.png';
import Wallet from "../../images/wallet";
import Idea from "../../images/idea";
import {Link} from "react-router-dom";


const Raiting = () => {
    return (
        <section className='raiting-container'>
            <div className="raiting-wrapper">
                <h1>Рейтинг благотворителей</h1>
                <div className="card-wrapper">
                    <div className="raiting-card">
                        <div className='author-info-container'>
                            <img src={photo1} alt=""/>
                            <div className="author-info-text">
                                <h5>Дима Иванов</h5>
                                <p>1 место</p>
                            </div>
                        </div>
                        <div className='info-container'>
                            <div className='info-wrapper' >
                                <div className="raiting-icon">
                                    <Wallet/>
                                </div>
                                <div className="raiting-count">
                                    <h6>100k</h6>
                                    <p>UAH</p>
                                </div>
                            </div>
                            <div className='info-wrapper'>
                                <div className="raiting-icon">
                                    <Idea/>
                                </div>
                                <div className="raiting-count">
                                    <h6>20</h6>
                                    <p>Проектов</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="raiting-card">
                        <div className='author-info-container'>
                            <img src={photo2} alt=""/>
                            <div className="author-info-text">
                                <h5>Дима Иванов</h5>
                                <p>2 место</p>
                            </div>
                        </div>
                        <div className='info-container'>
                            <div className='info-wrapper' >
                                <div className="raiting-icon">
                                    <Wallet/>
                                </div>
                                <div className="raiting-count">
                                    <h6>100k</h6>
                                    <p>UAH</p>
                                </div>
                            </div>
                            <div className='info-wrapper'>
                                <div className="raiting-icon">
                                    <Idea/>
                                </div>
                                <div className="raiting-count">
                                    <h6>20</h6>
                                    <p>Проектов</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="raiting-card">
                        <div className='author-info-container'>
                            <img src={photo3} alt=""/>
                            <div className="author-info-text">
                                <h5>Дима Иванов</h5>
                                <p>3 место</p>
                            </div>
                        </div>
                        <div className='info-container'>
                            <div className='info-wrapper' >
                                <div className="raiting-icon">
                                    <Wallet/>
                                </div>
                                <div className="raiting-count">
                                    <h6>100k</h6>
                                    <p>UAH</p>
                                </div>
                            </div>
                            <div className='info-wrapper'>
                                <div className="raiting-icon">
                                    <Idea/>
                                </div>
                                <div className="raiting-count">
                                    <h6>20</h6>
                                    <p>Проектов</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button className='primary-btn'><Link to='/raiting'>Посмотреть рейтинг</Link></button>
            </div>
        </section>
    )
};
export default Raiting;