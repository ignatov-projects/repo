import React from 'react';
import ReactDOM from 'react-dom';
import deloite from '../../images/deloitte.png';

import InfiniteCarousel from 'react-leaf-carousel';

const Carousel = () => {
    return(

    <InfiniteCarousel
        breakpoints={[
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
        ]}
        dots={true}
        showSides={true}
        sidesOpacity={.5}
        sideSize={.1}
        slidesToScroll={4}
        slidesToShow={4}
        scrollOnDevice={true}
    >
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
        <div className='carousel-item'>
            <img
                alt=''
                src={deloite}
            />
        </div>
    </InfiniteCarousel>
    );
}
export default Carousel;