import React from 'react';
import SumPeople from "../../../../images/SumPeople";
import SumWallet from "../../../../images/SumWallet";
import SumNeed from "../../../../images/SumNeed";
import SumCalendar from "../../../../images/SumCalendar";
import Partners from "../../partners";
import PaymentSystem from "../../../payment-system/payment-system";
import ReactPlayer from 'react-player'
import "video-react/dist/video-react.css";
import BenefactorsContainer from "./projects-benefactors/projects-benefactors-container";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

const ProjectDetails = (props) => {

            if(props.project){
                let raised =  props.project.moneyRaised >= 1000 ? props.project.moneyRaised/1000 : props.project.moneyRaised;
                let goal = props.project.projectMoneyGoal >= 1000 ? props.project.projectMoneyGoal/1000 : props.project.projectMoneyGoal;
                let needed = goal - raised;

                let projectDate = new Date(props.project.deadline.split('.').reverse());

                const timeNameConverter = (date) => {
                    let nowDate = new Date();
                    let differenceDate = date - nowDate;

                    const names = {
                        seconds: {
                            one: 'Секунда',
                            some: 'Секунды',
                            many: 'Секунд'
                        },
                        minutes: {
                            one: 'Минута',
                            some: 'Минуты',
                            many: 'Минут'
                        },
                        hours: {
                            one: 'Час',
                            some: 'Часа',
                            many: 'Часов'
                        },
                        days: {
                            one: 'День',
                            some: 'Дня',
                            many: 'Дней'
                        }
                    };

                    let chooseName = (date, timeName) => {
                        let dateString = date.toString();
                        let lastSimbol = dateString[dateString.length-1];

                        let lastSimbols;
                        if (dateString.length > 1) {
                            lastSimbols = dateString[dateString.length-2]+lastSimbol;
                        }

                        if (lastSimbols !== '11' && lastSimbol === '1') {
                            return names[timeName]['one']
                        } else if ((lastSimbol === '2' || lastSimbol === '3' || lastSimbol === '4') &&
                            (lastSimbols !== '12' && lastSimbols !== '13' && lastSimbols !== '14' )) {
                            return names[timeName]['some']
                        } else {
                            return names[timeName]['many']
                        }
                    };

                    if (differenceDate > 86400000) {
                        let date = Math.round(differenceDate/(1000*60*60*24));
                        let timeName = chooseName(date, 'days');
                        return (
                            <div className="summary-info">
                                <h5>{date}</h5>
                                <p>{timeName}</p>
                            </div>
                        )
                    } else if (86400000 > differenceDate && differenceDate > 3600000) {
                        let date = Math.round(differenceDate/(1000*60*60));
                        let timeName = chooseName(date, 'hours');
                        return (
                            <div className="summary-info">
                                <h5>{date}</h5>
                                <p>{timeName}</p>
                            </div>
                        )
                    } else if (3600000 > differenceDate && differenceDate > 60000) {
                        let date = Math.round(differenceDate/(1000*60));
                        let timeName = chooseName(date, 'minutes');
                        return (
                            <div className="summary-info">
                                <h5>{date}</h5>
                                <p>{timeName}</p>
                            </div>
                        )
                    } else if (differenceDate < 60000 && differenceDate > 0) {
                        let date = Math.round(differenceDate/(1000));
                        let timeName = chooseName(date, 'seconds');
                        return (
                            <div className="summary-info">
                                <h5>{date}</h5>
                                <p>{timeName}</p>
                            </div>
                        )
                    } else if (differenceDate <= 0) {
                        if(props.project.type === 'active'){
                            props.archiveProject(props.id);
                        }
                        return (
                            <div className="summary-info">
                                <h5>0</h5>
                                <p>Дней</p>
                            </div>
                        )
                    }
                };



                let moneyRaised = () => {
                    if (raised >= 1000)
                        return Math.round(10 * raised / 1000)/10 + 'm';
                    else if (raised > 1)
                        return Math.round(10 * raised )/10 + 'k';
                    else return raised
                };

                let neededMoney = () => {
                    if (needed >= 1000)
                        return  Math.round(10 * needed / 1000)/10 + 'm';
                    else if (needed > 1)
                        return Math.round(10 * needed )/10 + 'k';
                    else if (needed < 1)
                        return 0;
                    else return needed
                };
                return (
                    <section className='about-project-container'>
                        {
                            props.showPayment ? <PaymentSystem handleMoneyChange={props.handleMoneyChange}
                                                               value={props.value}
                                                               cardNumber={props.cardNumber}
                                                               handleAnonymousChange={props.handleAnonymousChange}
                                                               submitPayment={props.submitPayment}
                                                               closeBtn={props.togglePayment}/> : null
                        }
                        <div className="bg-project-img">

                            { props.project.projectVideo !== '' ? null : <div className="project-opacity"> </div>}
                            <div className="main-img-container">
                                {
                                    props.project.projectVideo !== '' ?
                                        <ReactPlayer
                                            playsinline
                                            url={props.project.projectVideo}
                                            playing
                                            width='100%'
                                            height='760px'
                                            controls={true}
                                        />
                                        : (<><h1>{props.project.projectName}</h1> <img src={props.project.projectImages[0]}/></>)
                                }
                            </div>
                        </div>
                        <div className="project-desc-container">
                            <div className="summary-container">
                                <div className="summary">
                                    <div className="summary-item">
                                        <SumPeople/>
                                        <div className="summary-info">
                                            <h5>{props.project.donationsCount}</h5>
                                            <p>Людей</p>
                                        </div>
                                    </div>
                                    <div className="summary-item">
                                        <SumWallet/>
                                        <div className="summary-info">
                                            <h5>{ moneyRaised() }</h5>
                                            <p>{props.project.projectMoneyCurrency}</p>
                                        </div>
                                    </div>
                                    <div className="summary-item">
                                        <SumNeed/>
                                        <div className="summary-info">
                                            <h5>{ neededMoney() }</h5>
                                            <p>Нужно</p>
                                        </div>
                                    </div>
                                    <div className="summary-item">
                                        <SumCalendar/>
                                        {timeNameConverter(projectDate)}
                                    </div>
                                </div>
                            </div>
                            <div className="project-desc-about">
                                <h3>{props.project.projectName}</h3>
                                <div className="project-desc-wrapper">
                                    <div className="project-desc-img">
                                        <Carousel>
                                            {
                                                props.project.projectImages && props.project.projectImages.map(img => {
                                                    return <img src={img} alt="image"/>
                                                })
                                            }
                                        </Carousel>
                                    </div>
                                    <div className='project-desc-r-side'>
                                        <div className="project-desc-text">
                                            <p>{props.project.projectDescription}</p>
                                        </div>
                                    </div>
                                </div>
                                {
                                    props.project.type !== 'archive' ?
                                        <button onClick={props.togglePayment} className='primary-btn'>
                                            Помочь проекту
                                        </button> : null
                                }


                            </div>
                            <Partners/>
                            <BenefactorsContainer projectId={props.id} iterator={props.iterator}/>
                        </div>
                    </section>
                )
            } else {
                return (
                    <div style={{position: 'absolute', top: '50%', left: '50%'}}><p>Project is loading</p></div>
                )
            }

};

export default ProjectDetails;