import React from 'react';
import {compose} from "redux";
import {connect} from "react-redux";
import {firestoreConnect} from "react-redux-firebase";
import BenefactorsList from "../projects-benefactors/projects-benefactors-list";
import PrevBtn from "../../../../../images/prev-btn";
import NextBtn from "../../../../../images/next-btn";

class BenefactorsContainer extends React.Component {
    render() {
        const { donations } = this.props;
        return (
            <div className="benefactors-container">
                <h2>
                    Благотворители проекта
                </h2>
                <BenefactorsList
                    donations={donations}
                    projectId={this.props.projectId}
                    iterator={this.props.iterator}
                />
                <div className="benefactors-buttons">
                    <div className="prev-btn">
                        <PrevBtn/>
                    </div>
                    <div className="next-btn">
                        <NextBtn/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        donations: state.firestore.ordered.donations
    }
};

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'donations' }
    ])
)(BenefactorsContainer);