import React from 'react';
import Wallet from "../../../../../images/wallet";
import Calendar from "../../../../../images/Calendar";

const Benefactors = ({donation}) => {
    var date = new Date(donation.donateDate.toDate());

    var optionDay = {
        day: 'numeric'
    };
    var optionMonth = {
        month: 'long'
    };

    let donatedMoney = () => {
        if (donation.donatedMoney >= 1000000)
            return  Math.round(10 * donation.donatedMoney / 1000000)/10 + 'm';
        else if (donation.donatedMoney >= 1000)
            return Math.round(10 * donation.donatedMoney / 1000)/10 + 'k';
        else return donation.donatedMoney
    };
    return (
        <div className="benefactor-card">
            <div className="benefactor-avatar">
                {
                    donation.avatar ? <img src={donation.avatar}/> : donation.initials
                }
            </div>
            <div className="benefactor-name">
                {
                    donation.companyName ? <h5>{donation.companyName}</h5> : <h5>{donation.firstName} {donation.lastName}</h5>
                }
            </div>
            <div className="about-donate">
                <div className="donate-money">
                    <div className="donate-m-icon">
                        <Wallet/>
                    </div>
                    <div className="donate-text">
                        <h6>
                            {donatedMoney()}
                        </h6>
                        <p>
                            UAH
                        </p>
                    </div>
                </div>
                <div className="donate-date">
                    <div className="donate-d-icon">
                        <Calendar/>
                    </div>
                    <div className="donate-text">
                        <h6>
                            {date.toLocaleDateString('ru', optionDay)}
                        </h6>
                        <p>
                            {date.toLocaleDateString('ru', optionMonth)}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default Benefactors;