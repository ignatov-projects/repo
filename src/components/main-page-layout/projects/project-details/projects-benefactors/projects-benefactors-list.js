import React from 'react';
import Benefactors from "../projects-benefactors/projects-benefactors";

const BenefactorsList = ({donations, projectId, iterator}) => {
    let counter = 0;
    return (
        <div className="benefactors">
            {/* eslint-disable-next-line array-callback-return */}
            {donations && donations.map(donation => {
                if (donation.projectId === projectId && donation.anonymous === false && counter < iterator)
                {
                    counter++;
                    return (
                        <Benefactors donation={donation}
                                 key={donation.id}
                        />
                    )
                }

            })}
        </div>
    )
};

export default BenefactorsList;