import React from 'react';
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import ProjectDetails from "./project-details";
import {donate, archiveProject} from "../../../actions/projectActions";
import SignInPopup from '../../../pop-up_windows/sign-in/sign-in'

class ProjectDetailsContainer extends React.Component{
    state = {
        showPayment: false,
        value: '',
        cardNumber: '',
        anonymous: false,
        currency: 'uah',
        isPlaying: false,
        isAuth: true,
        iterator: 8,
        id: ''
    };

    togglePayment = (e) => {
        e.preventDefault();
        if(this.props.auth.uid){
            this.setState({
                showPayment: !this.state.showPayment,
                id: e.target.id,
                isAuth: true
            })
        } else {
            this.setState({
                isAuth: false,
                showJoinPopup: true
            })
        }
    };

    handleMoneyChange = (e) => {
        e.preventDefault();
        this.setState({
            [e.target.id]: e.target.value
        });
    };

    handleAnonymousChange = () => {
        this.setState({
            anonymous: !this.state.anonymous
        });
    };

    toggleJoinPopup = () => {
        this.setState({
            showJoinPopup: !this.state.showJoinPopup
        })
    };

    submitPayment = (e) => {
        e.preventDefault();
        const projectId = this.props.id;
        const donatedMoney = this.state.value;
        const currency = this.state.currency;
        const anonymous = this.state.anonymous;
        const moneyRaised = this.props.project.moneyRaised;
        const donationsCount = this.props.project.donationsCount + 1;
        const donateId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        const uid = this.props.auth.uid;
        var avatar;
        var initials;
        var donateProject;
        if (this.props.profile.firstName === undefined){
            const companyName = this.props.profile.companyName;
            initials = this.props.profile.initials;
            donateProject = { projectId, uid, initials, companyName, donatedMoney, currency, anonymous };
        } else if (this.props.profile.companyName === undefined){
            var firstName = this.props.profile.firstName;
            var lastName = this.props.profile.lastName;
            avatar = this.props.profile.avatar;
            donateProject = { projectId, uid, avatar, firstName, lastName, donatedMoney, currency, anonymous };
        }
        console.log('count: ', donationsCount);
        this.props.donate(donateProject, donateId, projectId, moneyRaised, donatedMoney, donationsCount);
    };

    handlePlay = () => {
        if (this.props.project.projectVideo !== ''){
            this.setState({
                isPlaying: !this.state.isPlaying
            })
        }
    };

    render() {
        const {project} = this.props;
        return (
            <div>
                {
                    this.state.isAuth === false && this.state.showJoinPopup === true ? <SignInPopup closePopup={this.toggleJoinPopup}/> : null
                }
                <ProjectDetails handleMoneyChange={this.handleMoneyChange}
                                value={this.state.value}
                                cardNumber={this.state.cardNumber}
                                handleAnonymousChange={this.handleAnonymousChange}
                                submitPayment={this.submitPayment}
                                togglePayment={this.togglePayment}
                                showPayment={this.state.showPayment}
                                project={project}
                                isPlaying={this.state.isPlaying}
                                handlePlay={this.handlePlay}
                                id={this.props.id}
                                iterator={this.state.iterator}
                                archiveProject={this.props.archiveProject}
                />
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        donate: (donateProject, donateId, projectId, money, value, donationsCount) => dispatch(donate(donateProject, donateId, projectId, money, value, donationsCount)),
        archiveProject: (id) => dispatch(archiveProject(id))
    }
};

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const projects = state.firestore.data.projects;
    const project = projects ? projects[id] : null;
    return {
        project: project,
        id: id,
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(ProjectDetailsContainer);