import React from 'react';
import CloseBtn from "../../../../../../images/close-btn";

const ImagePopUp = ({imageUrl, showImage}) => {
    return (
        <section className='new-project-form-container'>
            <div className="new-project-form-wrapper">
                <i className='close-btn' onClick={ showImage }>
                    <CloseBtn/>
                </i>
                <img src={imageUrl} alt="image"/>
            </div>
        </section>
    )
};

export default ImagePopUp;