import React, {Component} from 'react';
import {connect} from "react-redux";
import ProjectRequestList from "../project-request-list/project-request-list";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import {updateProject, editProject, deleteImage, deleteVideo} from "../../../../actions/projectActions";

class NewProjects extends Component{

    state = {
        showDeclinePopup: false,
        id: '',
        name: '',
        email: '',
        isEdit: false,
        isOpenImage: false,
        imageUrl: '',
        isOpenVideo: false,
        videoUrl: '',
        projectName: '',
        projectMoneyGoal: '',
        deadline: '',
        projectDescription: ''

    };

    toggleDeclinePopup = (e) => {
        e.preventDefault();
        this.setState({
            showDeclinePopup: !this.state.showDeclinePopup,
            id: e.target.id,
            name: e.target.name,
            email: e.target.value
        });
    };

    applyProject = (e) => {
        e.preventDefault();
        return this.props.updateProject(e.target.id);
    };

    showEdit = (e) => {
        e.preventDefault();
        const projects = this.props.projects;
        const id = e.target.id;
        let project = projects.filter((proj) => { return proj.id === id});
        project = project[0];
        this.setState({
            isEdit: !this.state.isEdit,
            id: e.target.id,
            projectName: project.projectName,
            projectMoneyGoal: project.projectMoneyGoal,
            deadline: project.deadline,
            projectDescription: project.projectDescription
        });
    };

    showImage = (image) => {
        this.setState({
            isOpenImage: !this.state.isOpenImage,
            imageUrl: image
        })
    };

    showVideo = (video) => {
        this.setState({
            isOpenVideo: !this.state.isOpenVideo,
            videoUrl: video
        })
    };

    editChange = (e) => {
        e.preventDefault();
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    deleteProjectImage = (e) => {
        let idx = e.target.id;
        console.log(222, idx);
        return this.props.deleteImage(idx, this.state.id);

    };

    deleteProjectVideo = (e) => {
        let idx = e.target.id;
        console.log(222, idx);
        return this.props.deleteVideo(idx, this.state.id);

    };

    handleSubmitEdit = (e) => {
        e.preventDefault();
        const id = this.state.id;
        const projectName = this.state.projectName;
        const projectMoneyGoal = this.state.projectMoneyGoal;
        const deadline = this.state.deadline;
        const projectDescription = this.state.projectDescription;
        this.setState({
            isEdit: !this.state.isEdit
        });
        return this.props.editProject(id, projectName, projectMoneyGoal, deadline, projectDescription)
    };


    render(){
        const { projects } = this.props;
        return (
            <ProjectRequestList
                id={this.state.id}
                name={this.state.name}
                email={this.state.email}
                showDeclinePopup={this.state.showDeclinePopup}
                apply={this.applyProject}
                decline={this.toggleDeclinePopup}
                projects={projects}
                showEdit={this.showEdit}
                openEdit={this.state.isEdit}
                editChange={this.editChange}
                handleSubmitEdit={this.handleSubmitEdit}
                deleteProjectImage={this.deleteProjectImage}
                deleteProjectVideo={this.deleteProjectVideo}
                showImage={this.showImage}
                isOpenImage={this.state.isOpenImage}
                imageUrl={this.state.imageUrl}
                showVideo={this.showVideo}
                isOpenVideo={this.state.isOpenVideo}
                videoUrl={this.state.videoUrl}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        projects: state.firestore.ordered.projects
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateProject: (project) => dispatch(updateProject(project)),
        editProject: (id, projectName, projectMoneyGoal, deadline, projectDescription) => dispatch(editProject(id, projectName, projectMoneyGoal, deadline, projectDescription)),
        deleteImage: (idx, id) => dispatch(deleteImage(idx, id)),
        deleteVideo: (idx, id) => dispatch(deleteVideo(idx, id))
    }
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(NewProjects);