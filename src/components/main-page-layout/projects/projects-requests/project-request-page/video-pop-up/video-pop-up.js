import React from 'react';
import CloseBtn from "../../../../../../images/close-btn";
import ReactPlayer from 'react-player'

const VideoPopUp = ({videoUrl, showVideo}) => {
    return (
        <section className='new-project-form-container'>
            <div className="new-project-form-wrapper video-wrapper">
                <i className='close-btn' onClick={ showVideo }>
                    <CloseBtn/>
                </i>
                <ReactPlayer
                    playsInline
                    url={videoUrl}
                    width='100%'
                    playing
                    controls={true}
                />
            </div>
        </section>
    )
};

export default VideoPopUp;