import React from 'react';
import moment from 'moment';
import Popup from "../../../../pop-up_windows/decline-popup/decline-popup";
import ImagePopUp from "../project-request-page/image-pop-up/image-pop-up";
import VideoPopUp from "../project-request-page/video-pop-up/video-pop-up";
import Play from "../../../../../images/play";

const ProjectRequest = ({project,
                            apply,
                            decline,
                            popup,
                            id,
                            name,
                            email,
                            openEdit,
                            showEdit,
                            editChange,
                            handleSubmitEdit,
                            deleteProjectImage,
                            deleteProjectVideo,
                            showImage,
                            isOpenImage,
                            imageUrl,
                            showVideo,
                            isOpenVideo,
                            videoUrl}) => {
    return (
            <div className="user-cabinet-page-wrapper">

                {
                    popup? <Popup id={id} name={name} email={email} closePopup={decline} /> : null
                }

                <div className="user-cabinet-page-content">
                    <div className="user-information">
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Название проекта:</h5></span>
                            <span className="information-row">
                                {
                                    openEdit && id===project.id ? (
                                        <div className='edit-input'>
                                            <textarea id='projectName' onChange={editChange} defaultValue={project.projectName} />
                                        </div>
                                    ) : <p>{project.projectName}</p>
                                }
                            </span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Деньги для сбора:</h5></span>
                            <span className="information-row">
                                {
                                    openEdit && id===project.id ? (
                                        <div className='edit-input'>
                                            <input type='number' id='projectMoneyGoal' onChange={editChange} defaultValue={project.projectMoneyGoal} />
                                            <select name="curency" id="">
                                                <option value="usd">USD</option>
                                                <option value="uah">UAH</option>
                                                <option value="rur">RUR</option>
                                            </select>
                                        </div>
                                    ) : <p>{project.projectMoneyGoal} {project.projectMoneyCurrency}</p>
                                }

                            </span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Дедлайн:</h5></span>
                            <span className="information-row">
                                {
                                    openEdit && id===project.id ? (
                                        <div className='edit-input'>
                                            <input type='text' id='deadline' onChange={editChange} defaultValue={project.deadline} />
                                        </div>
                                    ) : <p>{project.deadline}</p>
                                }
                            </span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Описание проекта:</h5></span><span className="information-row">
                                {
                                    openEdit && id===project.id ? (
                                        <div className='edit-input'>
                                            <textarea id='projectDescription' onChange={editChange} defaultValue={project.projectDescription} />
                                        </div>
                                    ) : <p>{project.projectDescription}</p>
                                }
                            </span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Ссылки на соцсети:</h5></span><span className="information-row">
                            {project.projectSocialLinks && project.projectSocialLinks.map(link => {
                                return (
                                    <p>
                                        <a href={'https://'+link.link} target='_blank' key={link.id}>
                                           { link.link }
                                        </a>
                                    </p>
                                )
                            })}

                            </span>
                        </div>

                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Имя:</h5></span><span className="information-row"><p>{project.authorFirstName}</p></span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Фамилия:</h5></span><span className="information-row"><p>{project.authorLastName}</p></span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Email:</h5></span><span className="information-row"><p>{project.authorEmail}</p></span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Номер телефона:</h5></span><span className="information-row"><p>{project.authorPhone}</p></span>
                        </div>
                        <div className="user-cabinet-row">
                            <span className="name-of-row"><h5>Дата подачи:</h5></span><span className="information-row"><p>{moment(project.projectDateApplied.toDate()).calendar()}</p></span>
                        </div>
                        <div className="user-cabinet-row">
                            {
                                project.projectImages && project.projectImages.map((image) => {
                                    return (
                                        <span className='project-img' key={image}>
                                            <img src={image} alt="image of project" onClick={() => showImage(image)}/>
                                            {
                                                openEdit && id===project.id ? (
                                                    <button className='close-img' id={image} onClick={deleteProjectImage}>
                                                    </button>
                                                ) : null
                                            }
                                        </span>
                                    )
                                })
                            }

                            {
                                isOpenImage ? <ImagePopUp imageUrl={imageUrl} showImage={showImage} /> : null
                            }

                            {
                                project.projectVideo !== '' ?

                                <span className="project-img vid">
                                    {
                                        openEdit && id===project.id ? (
                                            <button className='close-img' id={project.projectVideo} onClick={deleteProjectVideo}>
                                            </button>
                                        ) : null
                                    }
                                    <span className='play' onClick={() => showVideo(project.projectVideo)}><Play/></span>
                                </span> :
                                    null
                            }
                            {
                                isOpenVideo ? <VideoPopUp videoUrl={videoUrl} showVideo={showVideo} /> : null
                            }

                        </div>
                    </div>
                    {
                        openEdit && id===project.id ? (
                            <div className="accept-btn">
                                <button className='primary-btn' type='submit' onClick={handleSubmitEdit}>Сохранить изменения</button>
                            </div>
                        ) : (
                            <div className="project-request-buttons">
                                <button className='primary-btn' onClick={apply} id={project.id}>Опубликовать проект</button>
                                <button className='primary-btn' onClick={showEdit} id={project.id}>Редактировать проект</button>
                                <button className='discard-btn' onClick={decline}
                                        id={project.id}
                                        name={project.authorFirstName}
                                        value={project.authorEmail}>Отклонить</button>
                            </div>
                        )
                    }
                    </div>
            </div>
    )
};

export default ProjectRequest;