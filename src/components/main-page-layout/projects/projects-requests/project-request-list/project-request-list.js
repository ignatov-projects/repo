import React from 'react';
import ProjectRequest from "../project-request/project-request";

const ProjectRequestList = ({projects,
                                apply,
                                decline,
                                showDeclinePopup,
                                id,
                                name,
                                email,
                                openEdit,
                                showEdit,
                                editChange,
                                handleSubmitEdit,
                                deleteProjectImage,
                                deleteProjectVideo,
                                showImage,
                                isOpenImage,
                                imageUrl,
                                showVideo,
                                isOpenVideo,
                                videoUrl}) => {
    return (
        <section className='user-cabinet-page-container'>
            <h1>Новые проекты</h1>
            {projects && projects.map(project => {
                return ( project.type === 'request' ?
                    <ProjectRequest id={id}
                                    name={name}
                                    email={email}
                                    popup={showDeclinePopup}
                                    apply={apply}
                                    decline={decline}
                                    project={project}
                                    openEdit={openEdit}
                                    showEdit={showEdit}
                                    editChange={editChange}
                                    handleSubmitEdit={handleSubmitEdit}
                                    deleteProjectImage={deleteProjectImage}
                                    deleteProjectVideo={deleteProjectVideo}
                                    key={project.id}
                                    showImage={showImage}
                                    isOpenImage={isOpenImage}
                                    imageUrl={imageUrl}
                                    showVideo={showVideo}
                                    isOpenVideo={isOpenVideo}
                                    videoUrl={videoUrl}/> :
                        null
                )
            })}
        </section>
    )
};

export default ProjectRequestList;