import React from 'react';
import ArrowDown from "../../../../../images/arrow-down";
import $ from 'jquery';
import NewProjectPopup from "../../../../pop-up_windows/new_project_pop-up/new_project_pop-up-container";
import ProjectsList from "../projects-list/projects-list";
import {compose} from "redux";
import {connect} from "react-redux";
import {firestoreConnect} from "react-redux-firebase";
import {Link} from "react-router-dom";
import PaymentSystem from "../../../../payment-system/payment-system";
import { donate } from "../../../../actions/projectActions";
import SignInPopup from '../../../../pop-up_windows/sign-in/sign-in'

class Projects extends React.Component {
    state = {
        showPopup: false,
        showPayment: false,
        showJoinPopup: false,
        anonymous: false,
        isAuth: true,
        currency: 'uah',
        iterator: 4,
        value: '',
        id: ''
    };

    togglePayment = (e) => {
        e.preventDefault();
        if(this.props.auth.uid){
            this.setState({
                showPayment: !this.state.showPayment,
                id: e.target.id,
                isAuth: true
            })
        } else {
            this.setState({
                isAuth: false,
                showJoinPopup: true
            })
        }
    };

    toggleJoinPopup = () => {
        this.setState({
            showJoinPopup: !this.state.showJoinPopup
        })
    };

    handleMoneyChange = (e) => {
        e.preventDefault();
        this.setState({
            value: e.target.value
        });
    };

    sendPay = () => {
        const projectId = this.state.id;
        const donateId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        const uid = this.props.auth.uid;
        var avatar;
        var initials;
        var donateProject;
        const currency = this.state.currency;
        const anonymous = this.state.anonymous;
        const donatedMoney = this.state.value;
        const projects = this.props.projects;
        let project = projects.filter((proj) => { return proj.id === projectId});
        project = project[0];
        const moneyRaised = project.moneyRaised;
        if (this.props.profile.firstName === undefined){
            const companyName = this.props.profile.companyName;
            initials = this.props.profile.initials;
            donateProject = { projectId, uid, initials, companyName, donatedMoney, currency, anonymous };
        } else if (this.props.profile.companyName === undefined){
            var firstName = this.props.profile.firstName;
            var lastName = this.props.profile.lastName;
            avatar = this.props.profile.avatar;
            donateProject = { projectId, uid, avatar, firstName, lastName, donatedMoney, currency, anonymous };
        }
        const donationsCount = project.donationsCount+1;
        this.props.donate(donateProject, donateId, projectId, moneyRaised, donatedMoney, donationsCount);
    };

    submitPayment = (e) => {
        e.preventDefault();
        this.sendPay();
    };

    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    render() {
        const { projects } = this.props;
        const moreProjects = () => {
            let iterator = this.state.iterator;
            this.setState({iterator: iterator + 4});
        };

        $(document).ready(function () {
            $('.add-project-btn').click(function () {
                $('#body').css({'position': 'fixed'})
            });
            $('.close-btn').click(function () {
                $('#body').css({'position': 'unset'})
            })
        });
        return (
            <section className='projects-container'>
                {
                    this.state.showPayment ? <PaymentSystem handleMoneyChange={this.handleMoneyChange}
                                                       value={this.state.value}
                                                       submitPayment={this.submitPayment}
                                                       closeBtn={this.togglePayment}/> : null
                }
                {
                    this.state.isAuth === false && this.state.showJoinPopup === true ? <SignInPopup closePopup={this.toggleJoinPopup}/> : null
                }
                <h1>
                    Проекты
                </h1>
                <ProjectsList i={this.state.iterator}
                              projects={projects}
                              togglePayment={this.togglePayment}
                />
                <div className="buttons-wrapper">
                    <button className="primary-btn add-project-btn" onClick={this.togglePopup.bind(this)}>Добавить проект</button>
                    {this.state.showPopup ?
                        <NewProjectPopup
                            closePopup={this.togglePopup.bind(this)}
                        />
                        : null
                    }
                    <div className="more">
                        <button className='arrow-down' onClick={moreProjects}>
                            <ArrowDown/>
                        </button>

                        <Link to='/projects' className="mobile-more-btn secondary-btn">
                            Другие проекты
                        </Link>
                    </div>
                </div>
            </section>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        donate: (donateProject, donateId, projectId, money, value, donationsCount) => dispatch(donate(donateProject, donateId, projectId, money, value, donationsCount))
    }
};

const mapStateToProps = (state) => {
    return {
        projects: state.firestore.ordered.projects,
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(Projects);