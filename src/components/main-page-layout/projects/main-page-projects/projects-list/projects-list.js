import React from 'react';
import Project from "../project/project";

const ProjectsList = ({projects, i, togglePayment}) => {
    let counter = 0;
    return (
        <div className="projects-wrapper">
            {/* eslint-disable-next-line array-callback-return */}
            {projects && projects.map(project => {

                if (project.type === 'active' && counter < i)
                {
                    counter++;
                    return (
                            <Project project={project}
                                     key={project.id}
                                     togglePayment={togglePayment}
                            />
                        )
                }

            })}
        </div>
    )
};

export default ProjectsList;