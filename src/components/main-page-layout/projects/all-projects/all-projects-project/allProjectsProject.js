import React from 'react';
import {Link} from "react-router-dom";

const AllProjectsProject = ({project}) => {

    let raised =  project.moneyRaised >= 1000 ? project.moneyRaised/1000 : project.moneyRaised;
    let goal = project.projectMoneyGoal >= 1000 ? project.projectMoneyGoal/1000 : project.projectMoneyGoal;
    let needed = goal - raised;

    let moneyRaised = () => {
        if (raised >= 1000)
            return Math.round(10 * raised / 1000)/10 + 'm';
        else if (raised > 1)
            return Math.round(10 * raised )/10 + 'k';
        else return raised
    };

    let neededMoney = () => {
        if (needed >= 1000)
            return  Math.round(10 * needed / 1000)/10 + 'm';
        else if (needed > 1)
            return Math.round(10 * needed )/10 + 'k';
        else if (needed < 1)
            return 0;
        else return needed
    };

    let moneyGoal = () => {
        if (goal >= 1000)
            return  Math.round(10 * goal / 1000)/10 + 'm';
        else if (goal > 1)
            return goal + 'k';
        else return goal
    };
    return (
        <div className="main-project-card">
            { needed <= 0 ?
                <span className='financed'><p>Профинансировано</p></span> :
                <span className='not-funded'><p>Не профинансировано</p></span>
            }
            <div className="img-container">
                <img src={project.projectImages[0]} alt=""/>
            </div>
            <div className="main-project-card-wrapper">
                <div className="main-project-card-title">
                    <h4>{project.projectName}</h4>
                </div>
                <div className="stats">
                    <div className="stat-item">
                        <div className="count">{project.donationsCount}</div>
                        <p>Взносов</p>
                    </div>
                    <div className="stat-item">
                        <div className="count">{ moneyRaised() }</div>
                        <p>{project.projectMoneyCurrency}</p>
                    </div>
                    <div className="stat-item">
                        <div className="count">{ moneyGoal() }</div>
                        <p>Нужно</p>
                    </div>
                    <div className="stat-item">
                        <div className="count">{ neededMoney() }</div>
                        <p>Осталось</p>
                    </div>
                </div>
                <div className="actions">
                        <a className="help-btn more-details" href={'/about-project/'+project.id}>Подробнее</a>
                </div>
            </div>
        </div>
    )
};

export default AllProjectsProject;