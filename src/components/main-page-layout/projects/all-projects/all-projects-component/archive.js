import React from 'react';
import {compose} from "redux";
import {connect} from "react-redux";
import {firestoreConnect} from "react-redux-firebase";
import AllProjectsList from "../all-projects-list/all-projects-list";
import Select from 'react-select';

const options = [
    { value: 'active', label: 'Активные проекты' },
    { value: 'archive', label: 'Архивные проекты' }
];

class AllProjectsPage extends React.Component {

    state = {
        filter: 'active',
        label: 'Активные проекты'
    };

    filterChange = (value) => {
        this.setState({
            filter: value.value,
            label: value.label
        });
        console.log(this.state);
    };

    render () {
        const { filter } = this.state;

        const {projects} = this.props;

        return (
            <section className='projects-container all-projects-container'>
                <div className="archive-filter">
                    <Select
                        placeholder={this.state.label}
                        value={filter}
                        onChange={this.filterChange}
                        options={options}
                        isSearchable={false}
                        styles={{
                            option: () => ({ backgroundColor: 'white', color: '#333333', fontFamily: 'Montserrat', padding: '20px', cursor: 'pointer', ':hover': {color: '#D62121'} }),
                            control: () => ({ color: '#333333', fontFamily: 'Montserrat', display: 'flex', height: '100%', paddingLeft: '12px', cursor: 'pointer' })

                        }}
                    />
                </div>
                <div className="mobile-filter">
                    <select name="filter" id="filter">
                        <option value="active">Активные проекты</option>
                        <option value="archive">Архив проектов</option>
                    </select>
                </div>
                <h1>
                    Проекты
                </h1>
                    <AllProjectsList filter={this.state.filter} projects={projects}/>
            </section>
        )
    };
}

const mapStateToProps = (state) => {
    return {
        projects: state.firestore.ordered.projects
    }
};

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(AllProjectsPage);