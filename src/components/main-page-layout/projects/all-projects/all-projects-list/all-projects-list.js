import React from 'react';
import AllProjectsProject from "../all-projects-project/allProjectsProject";

const AllProjectsList = ({projects, filter}) => {
    return (
        <div className="archive-projects-wrapper">
            {projects && projects.map(project => {
                return ( project.type === filter ?
                        <AllProjectsProject project={project} key={project.id}/> :
                        null
                )
            })}
        </div>
    )
};

export default AllProjectsList;