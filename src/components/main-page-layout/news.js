import React from 'react';
import NewsImg from '../../images/news.png';
import {Link} from "react-router-dom";

const News = () => {
    return (
        <section className='news-container'>
            <div className="news-wrapper">
                <h1>
                Новости
                </h1>
                <div className="news-card-wrapper">
                    <div className="news">
                        <img src={NewsImg} alt=""/>
                        <div className="about-news-container">
                            <div className="text-container">
                                <h4>Мероприятие по професиональному
                                    самоопределению.</h4>
                                <div className="desc">
                                    <p>Сегодня в парке им.Горького прошло
                                        мероприятие для детей, где они могли
                                        почувствовать себя взрослыми.</p>
                                </div>
                            </div>
                            <div className="desc-btn">
                                Описание
                            </div>
                        </div>
                    </div>
                    <div className="news">
                        <img src={NewsImg} alt=""/>
                        <div className="about-news-container">
                            <div className="text-container">
                                <h4>Мероприятие по професиональному
                                    самоопределению.</h4>
                                <div className="desc">
                                    <p>Сегодня в парке им.Горького прошло
                                        мероприятие для детей, где они могли
                                        почувствовать себя взрослыми.</p>
                                </div>
                            </div>
                            <div className="desc-btn">
                                Описание
                            </div>
                        </div>
                    </div>
                    <div className="news">
                        <img src={NewsImg} alt=""/>
                        <div className="about-news-container">
                            <div className="text-container">
                                <h4>Мероприятие по професиональному
                                    самоопределению.</h4>
                                <div className="desc">
                                    <p>Сегодня в парке им.Горького прошло
                                        мероприятие для детей, где они могли
                                        почувствовать себя взрослыми.</p>
                                </div>
                            </div>
                            <div className="desc-btn">
                                Описание
                            </div>
                        </div>
                    </div>
                    <div className="news">
                        <img src={NewsImg} alt=""/>
                        <div className="about-news-container">
                            <div className="text-container">
                                <h4>Мероприятие по професиональному
                                    самоопределению.</h4>
                                <div className="desc">
                                    <p>Сегодня в парке им.Горького прошло
                                        мероприятие для детей, где они могли
                                        почувствовать себя взрослыми.</p>
                                </div>
                            </div>
                            <div className="desc-btn">
                                Описание
                            </div>
                        </div>
                    </div>
                </div>
                <button className="primary-btn">
                    <Link to='/news'>Показать все</Link>
                </button>
            </div>
        </section>
    )
};
export default News;