import React from 'react';
import IntroImg from '../../images/intro.png'

const Intro = () => {
    return (
        <section className='intro-container'>
            <div className="intro-left-container">
                <div className="intro-bg-left"></div>
            </div>

            <div className="intro">
                <img src={IntroImg} alt=""/>
            </div>
            <div className="intro-right-container">
                <div className="intro-bg-right">
                    <h2>
                        Здесь Вы имеете возможность оказать помощь людям, став благотворителем проекта.
                    </h2>
                </div>
            </div>
        </section>
    )
};
export default Intro;