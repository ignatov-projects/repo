import React from 'react';
import background from '../../images/img-bg.png';
import Month from "../../images/month-icon";
import Year from "../../images/year";
import PopupJoin from "../pop-up_windows/join_pop-up/join_pop-up";
import $ from 'jquery';
import {connect} from "react-redux";

class Join extends React.Component {
        state = {
            showPopup: false
        };

    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    render() {
        $(document).ready(function(){
            $('.join-btn, .login-icon').click(function () {
                $('#body').css({'position': 'fixed'})
            });
            $('.close-btn').click(function () {
                $('#body').css({'position': 'unset'})
            })
        });
        return (
            <section className='join-container' style={{
                backgroundImage: `url(${background})`,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'
            }}>
                <div className="join-wrapper">
                    <div className="join-content">
                        <h1>
                            Международная платформа для реализации благотворительных проектов
                            в партнёрстве с фондом Александра Гвоздика.
                        </h1>
                        {
                            this.props.auth.uid ? null : <button className="join-btn" onClick={this.togglePopup.bind(this)}>Присоединиться</button>
                        }

                        {this.state.showPopup ?
                            <PopupJoin
                                closePopup={this.togglePopup.bind(this)}
                            />
                            : null
                        }

                    </div>
                </div>
                <section className='total-container'>
                    <div className="total-wrapper">
                        <div className="month">
                            <Month/>
                            <h5>2 567 815 UAH</h5>
                            <p>Cобрано в текущем месяце</p>
                        </div>
                        <div className="year">
                            <Year/>
                            <h5>22 567 815 UAH</h5>
                            <p>Cобрано в текущем году</p>
                        </div>
                    </div>
                </section>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
}

export default connect(mapStateToProps)(Join);