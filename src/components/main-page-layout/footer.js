import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {

    return (
        <footer>
            <div className="nav-links">
                <div className="footer-links-block">
                    <Link to='/' className="link">О платформе</Link>
                    <Link to='/' className="link">Правило платформы</Link>
                    <Link to='/' className="link">Документы</Link>
                </div>
                <div className="footer-links-block">
                    <Link to='/news' className="link">Новости</Link>
                    <Link to='/' className="link">Публикации в СМИ</Link>
                    <Link to='/archive' className="link">Архив</Link>
                </div>
            </div>
        </footer>
    )
};

export default Footer;