import React from 'react';
import { Link } from 'react-router-dom';
import Facebook from "../../social/facebook";
import Twitter from "../../social/twitter";
import Instagram from "../../social/instagram";
import Youtube from "../../social/youtube";
import MenuIcon from "../../images/menu-icon";
import LogIn from "../../images/log-in";
import SignInPopup from "../pop-up_windows/sign-in/sign-in";
import $ from "jquery";
import PopupJoin from "../pop-up_windows/join_pop-up/join_pop-up";
import NewProjectPopup from "../pop-up_windows/new_project_pop-up/new_project_pop-up-container";
import SignedOutLink from "../signOutLink/signOutLink";
import { connect } from "react-redux";
import MobileSignedOutLink from "../signOutLink/mobileSignOutLink";
import Select from 'react-select';


const currencyOptions = [
    { value: 'uah', label: 'UAH' },
    { value: 'rur', label: 'RUR' },
    { value: 'usd', label: 'USD' }
];

const localeOptions = [
    { value: 'ua', label: 'Укр' },
    { value: 'ru', label: 'Рус' },
    { value: 'en', label: 'Eng' }
];

class Header extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            showPopup: false,
            showMenu: false,
            currencyFilter: 'uah',
            currencyLabel: 'UAH',
            localeFilter: 'ru',
            localeLabel: 'Рус'
        };
    }

    toggleMenu() {
        this.setState({
            showMenu: !this.state.showMenu
        })
        console.log(this.state.showMenu)
    }

    toggleCloseMenu = () => {
        this.setState({
            showMenu: false
        })
    }

    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    toggleJoinPopup() {
        this.setState({
            showJoinPopup: !this.state.showJoinPopup
        });
    }

    toggleNewProjectPopup() {
        this.setState({
            showNewProjectPopup: !this.state.showNewProjectPopup
        });
    }

    currencyFilterChange = (value) => {
        this.setState({
            currencyFilter: value.value,
            currencyLabel: value.label
        });
        console.log(this.state);
    };

    localeFilterChange = (value) => {
        this.setState({
            localeFilter: value.value,
            localeLabel: value.label
        });
        console.log(this.state);
    };

    render() {

        const { auth, profile } = this.props;
        const link = auth.uid ?
            <SignedOutLink
                isMenu={this.state.showMenu}
                toggleMenu={this.toggleMenu.bind(this)}
                toggleCloseMenu={this.toggleCloseMenu.bind(this)}
                profile={profile} />
            :
            <i className="login-icon" onClick={this.togglePopup.bind(this)}>
                <LogIn/>
            </i>;



        $(document).ready(function(){
            $('.login-icon, #link').click(function () {
                $('#body').css({'position': 'fixed'})
            });
            $('.close-btn').click(function () {
                $('#body').css({'position': 'unset'})
            });
//----------------------------------------------------------------------//

            var flag = true;

            $('.menu-icon').click(function () {
                if (flag===true) {
                    $('.mobile-menu-burger-container').css({'left': '0'});
                    flag = false;
                }
                else {
                    $('.mobile-menu-burger-container').css({'left': '-990px'});
                    flag = true;
                }
            });

            $('.mobile-m-b-item').click(function () {
                var menuToLeft = function (){
                        $('.mobile-menu-burger-container').css({'left': '-990px'});
                        flag = true;
                };
                setTimeout(menuToLeft, 300)
            });

        });
        const {currencyFilter} = this.state;
        const {localeFilter} = this.state;

        return (
            <header>
                {
                    this.state.showJoinPopup ?
                    <PopupJoin
                        closePopup={this.toggleJoinPopup.bind(this)}
                    />
                    : null
                }

                {
                    this.state.showNewProjectPopup ?
                    <NewProjectPopup
                        closePopup={this.toggleNewProjectPopup.bind(this)}
                    />
                    : null
                }

                {
                    this.state.showPopup ?
                    <SignInPopup
                        closePopup={this.togglePopup.bind(this)}
                    />
                    : null
                }

                {
                    this.state.showPopup && auth.uid ?
                        window.location.reload() :
                        null
                }

                <div className="header-wrapper">
                    <a href='/'>
                        <div className="logo">
                            logo
                        </div>
                    </a>
                    <div className="nav-links">
                        <Link to='/projects' className="link">Проекты</Link>
                        {
                            auth.uid ? null :
                            <Link to='#' id='link' className="link" onClick={this.toggleJoinPopup.bind(this)}>Стать благотворителем</Link>
                        }

                        <Link to='/raiting' className="link">Рейтинг</Link>
                        <Link to='#' id='link' className="link" onClick={this.toggleNewProjectPopup.bind(this)}>Добавить проект</Link>

                        <Link to='/' className="link">О платформе</Link>
                    </div>
                    <div className="menu-icon">
                        <h6>Меню</h6>
                        <MenuIcon/>
                    </div>
                    <div className="social-links">
                        <a href="https://twitter.com" target='_blank'><Twitter/></a>
                        <a href="https://instagram.com" target='_blank'><Instagram/></a>
                        <a href="https://youtube.com" target='_blank'><Youtube/></a>
                        <a href="https://facebook.com" target='_blank'><Facebook/></a>
                    </div>
                    <div className="select-container">
                        <Select
                            placeholder={this.state.localeLabel}
                            value={localeFilter}
                            onChange={this.localeFilterChange}
                            options={localeOptions}
                            isSearchable={false}
                            styles={{
                                option: () => ({ backgroundColor: 'white', color: '#333333', fontFamily: 'Montserrat', fontWeight: '100', paddingTop: '5px', paddingBottom: '5px', display: 'flex', justifyContent: 'center', cursor: 'pointer', ':hover': {color: '#D62121'} }),
                                control: () => ({ color: '#ffffff', fontFamily: 'Montserrat', display: 'flex', height: '100%', cursor: 'pointer' }),
                                placeholder: () => ({ color: '#fff', fontWeight: '100', marginLeft: '2px', marginRight: '2px', position: 'absolute', top: '50%', transform: 'translateY(-50%)', boxSizing: 'border-box' }),
                                indicatorSeparator: () => ({ display: 'none' }),
                                indicatorsContainer: () => ({ paddingLeft: '0', width: '18px', fill: '#fff'})

                            }}
                        />
                        <Select
                            placeholder={this.state.currencyLabel}
                            value={currencyFilter}
                            onChange={this.currencyFilterChange}
                            options={currencyOptions}
                            isSearchable={false}
                            styles={{
                                option: () => ({ backgroundColor: 'white', color: '#333333', fontFamily: 'Montserrat', paddingTop: '5px', paddingBottom: '5px', display: 'flex', justifyContent: 'center', cursor: 'pointer', ':hover': {color: '#D62121'} }),
                                control: () => ({ color: '#ffffff', fontFamily: 'Montserrat', display: 'flex', height: '100%', cursor: 'pointer' }),
                                placeholder: () => ({ color: '#fff', fontWeight: '100', marginLeft: '2px', marginRight: '2px', position: 'absolute', top: '50%', transform: 'translateY(-50%)', boxSizing: 'border-box' }),
                                indicatorSeparator: () => ({ display: 'none' }),
                                indicatorsContainer: () => ({ paddingLeft: '0', paddingRight: '0', width: '18px'})

                            }}
                        />
                    </div>
                    { link }
                </div>
                <div className="mobile-menu-burger-container">
                    <div className="mobile-menu-burger-wrapper">
                        <ul>
                            <Link to='/projects' className='mobile-m-b-item'>Проекты</Link>
                            <Link to='#' className='mobile-m-b-item' onClick={this.toggleJoinPopup.bind(this)}>Стать благотворителем</Link>

                            <Link to='/raiting' className='mobile-m-b-item'>Рейтинг</Link>
                            <Link to='#' className='mobile-m-b-item' onClick={this.toggleNewProjectPopup.bind(this)} >Добавить проект</Link>
                            <li className='mobile-m-b-item'>О платформе</li>
                            <Link to='/cabinet' className='mobile-m-b-item'>Мой кабинет</Link>
                            { auth.uid ?
                                <MobileSignedOutLink/> :
                                <Link to='#' className='mobile-m-b-item' onClick={this.togglePopup.bind(this)}>Войти</Link>
                            }
                            <li className='mobile-m-b-item options-social-links'>
                                <select name="lang" id="lang" className='select'>
                                    <option value="rus">Рус</option>
                                    <option value="ukr">Укр</option>
                                    <option value="eng">Eng</option>
                                </select>
                                <select name="currency" id="currency" className='select'>
                                    <option value="UAH">UAH</option>
                                    <option value="RUR">RUR</option>
                                    <option value="USD">USD</option>
                                </select>
                                <div className="social-links">
                                    <a href="https://twitter.com" target='_blank'><Twitter/></a>
                                    <a href="https://instagram.com" target='_blank'><Instagram/></a>
                                    <a href="https://youtube.com" target='_blank'><Youtube/></a>
                                    <a href="https://facebook.com" target='_blank'><Facebook/></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        profile: state.firebase.profile,
        projects: state.firestore.data.projects
    }
};

export default connect(mapStateToProps)(Header);