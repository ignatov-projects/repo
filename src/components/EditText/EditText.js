import React from 'react';
import DraftEditor from "../DraftEditor/DraftEditor";

const EditTextPage = () => {
    return (
        <section className='text-page-container'>
            <h1>Текстовая страница</h1>
            <div className="text-page-wrapper">
                <div className="text-page-content">
                    <DraftEditor/>
                </div>
            </div>
        </section>
    )
};

export default EditTextPage;