import React, {Component} from "react";
import { Editor, EditorState, RichUtils} from 'draft-js';

export default class DraftEditor extends Component {
    constructor(props) {
        super(props);

        this.state = { editorState: EditorState.createEmpty() };
        this.onChange = (editorState) => this.setState({ editorState });
        this.handleKeyCommand = this.handleKeyCommand.bind(this);
        }
    handleKeyCommand(command, editorState) {
        const newState = RichUtils.handleKeyCommand(editorState, command);
        if (newState) {
            this.onChange(newState);
            return 'handled';
        }
        return 'not-handled';
    }

    render() {
        const { editorState } = this.state;

        return (
            <div
                id="editor-container"
                className="c-editor-container js-editor-container"
            >
                <div className="editor">
                    <Editor
                        editorState={editorState}
                        onChange={this.onChange}
                        handleKeyCommand={this.handleKeyCommand}
                        placeholder="Введите текст..."
                    />
                </div>
            </div>
        );
    }
}
