import React from 'react';
import CNN from '../../images/cnn.png'
import CAF from '../../images/caf.png'
import Deloitte from '../../images/deloitte.png'
import Qiwi from '../../images/qiwi.png'
import Tupperware from '../../images/Tupperware.png'

const PartnersPage = () => {
    return (
        <section className='partners-page-container'>
            <h1>Партнеры</h1>
            <div className="partners-page-wrapper">
                <div className="partners-page-content">
                    <div className="partner-icon-item">
                        <div className="partner-icon">
                            <img src={CNN} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={CAF} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={Deloitte} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={Qiwi} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={Tupperware} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={Deloitte} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={Qiwi} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={Tupperware} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={CNN} alt=""/>
                        </div>
                        <div className="partner-icon">
                            <img src={CAF} alt=""/>
                        </div>
                    </div>
                    <button className='primary-btn'>Добавить партнера</button>
                </div>
            </div>
        </section>
    )
};

export default PartnersPage;