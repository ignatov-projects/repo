import React from 'react';
import CloseBtn from "../../../images/close-btn";
import { connect } from "react-redux";
import {declineProject} from "../../actions/projectActions";
import {compose} from "redux";
import {firestoreConnect} from "react-redux-firebase";
import axios from "axios";

class Popup extends React.Component {

    state = {
        reason: ''
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const project = {
            author: this.props.name,
            reason: this.state.reason,
            email: this.props.email
        };

        axios.post('http://localhost:4000/send', {project}).then(
            res => {
                if(res.data.msg === 'success'){
                    alert('Message sent!');
                }else if (res.data.msg === 'fail'){
                    alert('Message failed to send!!!')
                }
            }
        );

        this.props.declineProject(this.props.id);
    };


    render() {
        return (
            <section className='reg-form-container'>
                <div className="reg-form-wrapper">
                    <i className='close-btn' onClick={this.props.closePopup}>
                        <CloseBtn/>
                    </i>
                    <h2>Причина отказа</h2>
                    <form onSubmit={this.handleSubmit} method='POST'>
                        <textarea onChange={this.handleChange} id="reason" cols="30" rows="10" placeholder='Опишите причину, по которой проект отклонен' />
                        <button type='submit' className='primary-btn' id={this.props.id}> Подтвердить </button>
                    </form>
                </div>
            </section>
        )
    };
}


const mapDispatchToProps = (dispatch) => {
    return {
        declineProject: (project) => dispatch(declineProject(project))
    }
};

export default compose(
    connect(null, mapDispatchToProps),
    firestoreConnect([
        { collection: 'projects' }
    ])
)(Popup);