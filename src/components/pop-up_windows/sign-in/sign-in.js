import React from 'react';
import CloseBtn from "../../../images/close-btn";
import {Link} from "react-router-dom";
import { connect } from "react-redux";
import { signIn } from '../../actions/authActions';
import PopupJoin from "../join_pop-up/join_pop-up";

class SignInPopup extends React.Component {

    state = {
        email: '',
        password: '',
        isRegistered: true
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signIn(this.state);
    };

    toggleRegisterPopup = () => {
        this.setState({
            isRegistered: !this.state.isRegistered
        })
    };

    render() {

        const { authError } = this.props;
        return (
            <section className='reg-form-container'>
                {this.state.isRegistered === false ? <PopupJoin closePopup={this.toggleRegisterPopup}/> : null}
                <div className="sign-in-wrapper">
                    <i className='close-btn' onClick={this.props.closePopup}>
                        <CloseBtn/>
                    </i>
                        <div className='sign-in'>
                            <h2>Вход</h2>
                            <div className="reg-form-content">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="mail">
                                        <div className="field">
                                            <label htmlFor="mail">E-Mail</label>
                                            <input type='text' name='email' id='email' onChange={this.handleChange}/>
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label htmlFor="password">Пароль</label>
                                        <input type='password' name='password' id='password' onChange={this.handleChange}/>
                                    </div>
                                    <div className="signed-in-links">
                                        <Link>Забыли пароль?</Link>
                                        <Link onClick={this.toggleRegisterPopup}>Еще не зарегистрировались?</Link>
                                    </div>
                                    <div className="buttons">
                                        <button className="primary-btn">Войти</button>
                                        <div className="error-text">
                                            { authError ? <p>{authError}</p> : null }
                                        </div>
                                        <div className="fast-login">
                                            <p>Быстрая авторизация</p>
                                            <div className="social-login-buttons">
                                                <button className="google-btn">Google</button>
                                                <button className="fb-btn">Facebook</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                 </div>
            </section>
        )
    };
}

const mapStateToProps = (state) => {
    return {
        authError: state.auth.authError
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (creds) => dispatch(signIn(creds))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInPopup);