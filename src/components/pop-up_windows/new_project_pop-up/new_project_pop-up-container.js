import React, {Component} from 'react';
import 'air-datepicker/dist/js/datepicker.js';
import 'air-datepicker/dist/css/datepicker.css';
import $ from 'jquery';
import {connect} from "react-redux";
import {createProject} from "../../actions/projectActions";
import {storage} from "../../../config/fbConfig";
import axios from "axios";
import NewProjectForm from "./new-project-form";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import resetOrientation from 'reset-image-orientation'

class NewProjectPopup extends Component {

    state = {
        projectId: '',
        projectName: '',
        authorFirstName: '',
        authorLastName: '',
        authorEmail: '',
        authorPhone: '',
        projectMoneyGoal: '',
        projectMoneyCurrency: 'UAH',
        moneyRaised: 0,
        donationsCount: 0,
        deadline: '',
        projectDescription: '',
        projectSocialLinks: [{link: ''}],
        projectImages: [],
        projectVideo: '',
        images: null,
        imagesNew: null,
        projectImagesPreview: [],
        type: 'request',
        benefactors: 0,
        size: 0,
        progress: 0,
        video: null,
        isImage: false
    };


///////////////////////////||||||||||||||||-----------------------------IMAGES--------------------------||||||||||||||||
    setRef = ref => {
        this.file = ref;
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    componentWillMount() {
        let projectId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        this.setState({
            projectId: projectId
        });
    }

    sendingEmail = () => {
        const userData = {
            author: this.state.authorFirstName,
            email: this.state.authorEmail
        };

        axios.post('http://localhost:4000/send/subscribe', {userData}).then(
            res => {
                if(res.data.msg === 'success'){
                    alert('Message sent!');
                    return window.location.reload();
                }else if (res.data.msg === 'fail'){
                    alert('Message failed to send!!!')
                }
            }
        );
    };


    handleImageChange = (e) => {
        e.preventDefault();
        let files = [...e.target.files];
        files.map(img => {
            resetOrientation(img, (base64) => {
                return img.src = base64;
            });
        });

        return new Promise ((resolve, reject) => {
            if (this.state.images !== null && this.state.imagesNew === null) {
                let images = [...this.state.images];
                    this.setState({
                        projectImagesPreview: [],
                        imagesNew: images.concat(...files),
                        isImage: true
                    });
                if (files.length > 0){
                    this.setState({
                        images: null,
                        isImage: true
                    });
                } else {
                    this.setState({
                        projectImagesPreview: [],
                        images: null,
                        isImage: true
                    })
                }
            } else if (this.state.imagesNew !== null && this.state.images === null) {
                let images = [...this.state.imagesNew];
                    this.setState({
                        projectImagesPreview: [],
                        images: images.concat(...files),
                        isImage: true
                    });
                if (files.length > 0){
                    this.setState({
                        imagesNew: null,
                        isImage: true
                    });
                } else {
                    this.setState({
                        projectImagesPreview: [],
                        imagesNew: null,
                        isImage: true
                    })
                }
            } else if (this.state.images === null && this.state.imagesNew === null) {
                this.setState({
                    images: files,
                    isImage: true
                });
            }
            resolve({})
        }).then(()=>{
            this.imagesPreview(files);
        });
    };

    imagesPreview = ()=> {
        if (this.state.imagesNew === null) {
            let images = this.state.images;
            Array.from(images).forEach(file => {
                const reader = new FileReader();
                reader.onload = (e) => {
                    this.setState({
                        projectImagesPreview: this.state.projectImagesPreview.concat(e.target.result)
                    });
                };
                reader.readAsDataURL(file);
            });
        } else if (this.state.images === null) {
            let images = this.state.imagesNew;
            Array.from(images).forEach(file => {
                const reader = new FileReader();
                reader.onload = (e) => {
                    this.setState({
                        projectImagesPreview: this.state.projectImagesPreview.concat(e.target.result)
                    });
                };
                reader.readAsDataURL(file);
            });
        }
    };

    /////////////////////REMOVE IMAGES////////////////

    handleCloseImage = (e)=> {
        e.preventDefault();
        return new Promise((resolve, reject) => {
            let idx = e.target.id;
            if (this.state.images !== null && this.state.imagesNew === null) {
                const images = [...this.state.images];
                images.splice(idx, 1);
                this.setState({
                    projectImagesPreview: [],
                    imagesNew: images,
                    images: null
                });
            } else if (this.state.images === null && this.state.imagesNew !== null) {
                const images = [...this.state.imagesNew];
                images.splice(idx, 1);
                this.setState({
                    projectImagesPreview: [],
                    images: images,
                    imagesNew: null
                });
            }
            resolve({})
        }).then(()=> {
            this.imagesPreview();
        });
    };

    ////////////////////////////

    uploadImage = () => {
        let images;
        const projectId = this.state.projectId;
        if (this.state.images !== null){
            images = this.state.images;
        } else {
            images = this.state.imagesNew;
        }
        return new Promise((resolve, reject) => {
            const storageRef = storage.ref(`projects/${projectId}/pictures`);
            let counter = 0;
            let length = Array.from(images).length;
            Array.from(images).forEach((img, idx) => {
                const mainImage = storageRef.child(img.name);
                mainImage.put(img).then((snapshot) => {
                    return (
                        mainImage.getDownloadURL().then(url => {
                            this.state.projectImages[idx] = url;
                            counter++;
                            if (counter === length){
                                return resolve({});
                            }
                        })
                    )
                })
            });
        });
    };

    handleVideoChange = (e) => {
        e.preventDefault();
        this.setState({
            video: e.target.files[0],
            size: e.target.files[0].size
        });
    };

    handleCloseVideo = (e) => {
        e.preventDefault();
        this.setState({
            video: null
        })
    };

    uploadVideo = () => {
        const projectId = this.state.projectId;
        return new Promise((resolve, reject) => {
            if (this.state.video !== null) {
                const video = this.state.video;
                const storageRef = storage.ref(`projects/${projectId}/video`);
                const mainVideo = storageRef.child(video.name);
                mainVideo.put(video).on('state_changed', (snapshot) => {
                    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    this.setState({
                        progress: Math.round(progress)
                    });
                }, (err) => {
                    console.log(err, 'error');
                    return reject({});
                }, () => {
                    mainVideo.getDownloadURL().then(url => {
                        this.setState({
                            projectVideo: url
                        });
                        return resolve({});
                    });
                });
            } else {
                this.setState({
                    projectVideo: '',
                    progress: 100
                });
                return resolve({});
            }
        }).catch(reject=>{
            console.log('no videoFile', reject)
        });
    };

    uploadData = () => {
        let uid = null;
        if (this.props.auth.uid){
            uid = this.props.auth.uid
        }
        const projectId = this.state.projectId;
        const project = {
            projectName: this.state.projectName,
            authorFirstName: this.state.authorFirstName,
            authorLastName: this.state.authorLastName,
            authorEmail: this.state.authorEmail,
            authorPhone: this.state.authorPhone,
            projectMoneyGoal: this.state.projectMoneyGoal,
            projectMoneyCurrency: this.state.projectMoneyCurrency,
            moneyRaised: this.state.moneyRaised,
            donationsCount: this.state.donationsCount,
            deadline: this.state.deadline,
            projectDescription: this.state.projectDescription,
            projectSocialLinks: this.state.projectSocialLinks,
            projectImages: this.state.projectImages,
            projectVideo: this.state.projectVideo,
            type: this.state.type,
            benefactors: this.state.benefactors
        };
        console.log('done!');
        this.props.createProject(project, projectId, uid);
        this.sendingEmail();
    };


    isImage = ()=> {
        if (this.state.projectImagesPreview.length === 0){
            this.setState({
                isImage: false
            });
        }
        if(this.state.isImage === false)
            alert('Не забудьте добавить изображения к своему проекту!');
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let isEmail = false;
        this.props.projects.map(project => {
            if (project.authorEmail === this.state.authorEmail && project.type === 'request'){
                isEmail = true;
            }
        });
        if (isEmail === true) {
            return alert('Заявка с указанным email еще в обработке')
        } else {
            return (
                this.uploadImage()
                    .then(() => {
                        this.uploadVideo()
                            .then(() => {
                                this.uploadData()
                            })
                    })
            );
        }
    };

    componentDidMount(){
        this.$el = $(this.el);
        this.$el.datepicker()
    }

    refEl = (el) => {
        this.el = el
    };

    //////////////////////-----SOCIAL LINKS

    handleShareholderNameChange = idx => e => {
        const newLink = this.state.projectSocialLinks.map((link, sidx) => {
            if (idx !== sidx) return link;
            return { ...link, link: e.target.value };
        });

        this.setState({ projectSocialLinks: newLink });
    };

    handleAddShareholder = () => {
        this.setState({
            projectSocialLinks: this.state.projectSocialLinks.concat([{ link: "" }])
        });
    };

    handleRemoveShareholder = idx => () => {
        this.setState({
            projectSocialLinks: this.state.projectSocialLinks.filter((s, sidx) => idx !== sidx)
        });
    };

    projectForm = () => {
        const percentage = this.state.progress;
        if (percentage < 1){
            return (
                <NewProjectForm
                    closePopup={this.props.closePopup}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                    el={this.refEl}
                    projectSocialLinks={this.state.projectSocialLinks}
                    handleShareholderNameChange={this.handleShareholderNameChange}
                    handleRemoveShareholder={this.handleRemoveShareholder}
                    handleAddShareholder={this.handleAddShareholder}
                    projectImagesPreview={this.state.projectImagesPreview}
                    handleCloseImage={this.handleCloseImage}
                    video={this.state.video}
                    handleCloseVideo={this.handleCloseVideo}
                    handleImageChange={this.handleImageChange}
                    setRef={this.setRef}
                    handleVideoChange={this.handleVideoChange}
                    isImage={this.isImage}
                />
            )
        } else if (percentage > 1 && percentage <= 100) {
            return (
                <div>
                    <div style={{filter: 'blur(5px)', position: 'relative', opacity: '.4'}}>
                        <NewProjectForm
                            closePopup={this.props.closePopup}
                            handleSubmit={this.handleSubmit}
                            handleChange={this.handleChange}
                            el={this.refEl}
                            projectSocialLinks={this.state.projectSocialLinks}
                            handleShareholderNameChange={this.handleShareholderNameChange}
                            handleRemoveShareholder={this.handleRemoveShareholder}
                            handleAddShareholder={this.handleAddShareholder}
                            projectImagesPreview={this.state.projectImagesPreview}
                            handleCloseImage={this.handleCloseImage}
                            video={this.state.video}
                            handleCloseVideo={this.handleCloseVideo}
                            handleImageChange={this.handleImageChange}
                            setRef={this.setRef}
                            handleVideoChange={this.handleVideoChange}
                        />
                    </div>
                    <div style={{position: 'fixed', width: '100px', top: '50%', left: '47%'}}>
                        <CircularProgressbar
                            value={percentage}
                            text={percentage+'%'}
                            styles={buildStyles({
                                textColor: 'D62121',
                                pathColor: 'D62121'})}
                        />
                    </div>
                </div>
            )
        }
    };

    render() {
        return (
            <section className='new-project-form-container'>
                <div className="new-project-form-wrapper">
                    {this.projectForm()}
                </div>
            </section>
        )
    };
}

const mapStateToProps = (state) => {
    return {
        projects: state.firestore.ordered.projects,
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        createProject: (project, projectId, uid) => dispatch(createProject(project, projectId, uid)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewProjectPopup) ;