import React from 'react'
import CloseBtn from "../../../images/close-btn";

const NewProjectForm = (props) => {
    return (
        <div>
                <i className='close-btn' onClick={props.closePopup}>
                    <CloseBtn/>
                </i>
                <h2>Добавить проект</h2>
                <div className="new-project-form-content">
                    <form onSubmit={props.handleSubmit}>
                        <div className="project-name">
                            <div className="field">
                                <label htmlFor="project-name">Название проекта</label>
                                <input type='text' name='project-name' id='projectName' required onChange={props.handleChange}/>
                            </div>
                        </div>
                        <div className="name">
                            <div className="field">
                                <label htmlFor="firstName">Имя</label>
                                <input type='text' name='firstName' id='authorFirstName' required pattern='^[A-z]{1,16}$' onChange={props.handleChange}/>
                            </div>
                            <div className="field">
                                <label htmlFor="lastName">Фамилия</label>
                                <input type='text' name='lastName' id='authorLastName' required pattern='^[A-z]{1,16}$' onChange={props.handleChange}/>
                            </div>
                        </div>
                        <div className="mail">
                            <div className="field">
                                <label htmlFor="mail">E-Mail</label>
                                <input type='text' name='mail' id='authorEmail' required pattern='/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i' onChange={props.handleChange}/>
                            </div>
                        </div>
                        <div className="contact">
                            <div className="field">
                                <label htmlFor="tel">Телефон</label>
                                <input type='text' name='tel' id='authorPhone' required pattern='^\+?(\d{1,3})?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$' onChange={props.handleChange}/>
                            </div>
                        </div>
                        <div className="money-deadline">
                            <div className="field">
                                <label htmlFor="money">Деньги для сбора</label>
                                <div className="money-to-collect">
                                    <input type='text' name='money' id='projectMoneyGoal' required pattern='^(?!0.*$)([0-9]{1,9}?)$' onChange={props.handleChange}/>
                                    <select name="currency" id="projectMoneyCurrency" className='select' onChange={props.handleChange}>
                                        <option selected value="UAH">UAH</option>
                                        <option value="RUR">RUR</option>
                                        <option value="USD">USD</option>
                                    </select>
                                </div>
                            </div>
                            <div className="field">
                                <label htmlFor="deadline">Дедлайн</label>
                                <input type="text"
                                       id='deadline'
                                       className="datepicker-here"
                                       required
                                       pattern='/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/'
                                       autoComplete='off'
                                       ref={el => props.el(el)}
                                       onBlur={props.handleChange}/>
                            </div>
                        </div>
                        <div className="project-description">
                            <div className="field">
                                <label htmlFor="project-description">Описание проекта</label>
                                <textarea name='project-description' id='projectDescription' required onChange={props.handleChange}/>
                            </div>
                        </div>
                        <div className="project-social-links">
                            <div className="field">
                                <label htmlFor="social-links">Ссылки на социальные сети проекта</label>

                                {props.projectSocialLinks.map((link, idx) => (
                                    <div className="project-social-links">
                                        <input
                                            type="text"
                                            placeholder={`Ссылка на соц. сеть`}
                                            value={link.link}
                                            onChange={props.handleShareholderNameChange(idx)}
                                        />
                                        <button
                                            type="button"
                                            onClick={props.handleRemoveShareholder(idx)}
                                            className="remove-link"
                                            style={{dackground: 'none'}}
                                        >
                                            -
                                        </button>
                                    </div>
                                ))}
                                <button
                                    type="button"
                                    onClick={props.handleAddShareholder}
                                    className="primary-btn"
                                    id='add-link'
                                >
                                    Добавить ссылку
                                </button>

                            </div>
                        </div>
                        <div className="images-preview">
                            {props.projectImagesPreview.map((image, idx) => (
                                <span className='project-img' key={image}>
                                        <img src={image} alt="image of project"/>
                                        <button className='close-img' id={idx} onClick={props.handleCloseImage}>
                                        </button>
                                    </span>
                            ))}
                        </div>
                        <div className="video-preview">
                            { props.video ?
                                <span className='project-img vid'>
                                            <span>{props.video.name}</span>
                                            <button className='close-img' onClick={props.handleCloseVideo}>
                                            </button>
                                        </span>
                                : null }
                        </div>
                        <div className="buttons">
                            <input type='file' multiple id='projectImages' className='inputfile' required accept="image/x-png,image/jpeg" onChange={ props.handleImageChange} ref={ref => props.setRef(ref)}/>
                            <label htmlFor="projectImages" className="google-btn">Выберите изображения</label>
                            <input type='file' id='projectVideo' className='inputfile' accept="video/mp4, video/avi" onChange={props.handleVideoChange} ref={ref => props.setRef(ref)}/>
                            <label htmlFor="projectVideo" className="google-btn">Выберите видео</label>
                            <button type='submit' className="primary-btn" onClick={()=>props.isImage()}>Добавить проект</button>
                        </div>
                    </form>
                </div>
            </div>
    )
};

export default NewProjectForm;