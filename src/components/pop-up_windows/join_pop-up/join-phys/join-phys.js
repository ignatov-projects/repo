import React from 'react';
import { connect } from "react-redux";
import { signUp } from "../../../actions/authActions";
import { storage } from "../../../../config/fbConfig";
import uploadAvatar from '../../../../images/choose-avatar.png'
import {getFirebase} from "react-redux-firebase";

class PopupJoinPhys extends React.Component {

    state = {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        password: '',
        country: '',
        city: '',
        createdProjects: [],
        avatar: null,
        type: 'person',
        avatarPreview: ''
    };

    setRef = ref => {
        this.file = ref;
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    handleImageChange = (e) => {
        this.setState({
            avatarPreview: ''
        });
        let img = e.target.files[0];
        const reader = new FileReader();
        if (e.target.files[0] !== undefined) {
            reader.onload = (e) => {
                this.setState({
                    avatarPreview: this.state.avatarPreview.concat(e.target.result)
                });
            };
            reader.readAsDataURL(img);
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let file = this.file.files[0];
        getFirebase().auth().createUserWithEmailAndPassword(
            this.state.email,
            this.state.password
        ).then((resp)=>{
            const storageRef = storage.ref(`users/${resp.user.uid}/avatar`);
            const mainImage = storageRef.child(this.file.files[0].name);
            mainImage.put(file).then(() => {
                mainImage.getDownloadURL().then((url) => {
                    let userData = {
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        email: this.state.email,
                        phone: this.state.phone,
                        country: this.state.country,
                        city: this.state.city,
                        avatar: url,
                        type: this.state.type,
                        createdProjects: this.state.createdProjects
                    };
                    this.props.signUp(userData, resp.user.uid);
                })
            });
        }).then(()=> window.location.reload())
    };

    render() {
        return (
            <div className="reg-form-content">
                <form className='join-form' onSubmit={(e)=>this.handleSubmit(e)}>

                    <div className="upload-avatar">
                        <input type="file" id='avatar' className='avatar' accept="image/x-png,image/jpeg" onChange={this.handleImageChange} ref={this.setRef} />
                        <label htmlFor="avatar" className='avatar-preview'>
                            {
                                this.state.avatarPreview !== '' ? (
                                    <span>
                                        <img src={this.state.avatarPreview} alt="choose-avatar"/>
                                        <p className='edit-avatar'>Сменить фото</p>
                                    </span>

                                    ) : <img src={uploadAvatar} alt="choose-avatar"/>
                            }
                        </label>
                    </div>

                    <div className="name">
                        <div className="field">
                            <label htmlFor="firstName">Имя</label>
                            <input type='text' name='firstName' id='firstName' onChange={this.handleChange} required/>
                        </div>
                        <div className="field">
                            <label htmlFor="lastName">Фамилия</label>
                            <input type='text' name='lastName' id='lastName' onChange={this.handleChange} required/>
                        </div>
                    </div>
                    <div className="mail">
                        <div className="field">
                            <label htmlFor="email">E-Mail</label>
                            <input type='text' name='email' id='email' onChange={this.handleChange} required/>
                        </div>
                    </div>
                    <div className="contact">
                        <div className="field">
                            <label htmlFor="phone">Телефон</label>
                            <input type='text' name='phone' id='phone' onChange={this.handleChange} required/>
                        </div>
                        <div className="field">
                            <label htmlFor="password">Пароль</label>
                            <input type='password' name='password' id='password' onChange={this.handleChange} required />
                        </div>
                        <div className="field">
                            <label htmlFor="country">Страна</label>
                            <input type='text' name='country' id='country' onChange={this.handleChange} required />
                        </div>
                        <div className="field">
                            <label htmlFor="city">Город</label>
                            <input type='text' name='city' id='city' onChange={this.handleChange} required/>
                        </div>
                    </div>

                    <div className="buttons">
                        <button className="primary-btn">Зарегистрироваться</button>
                        {/*<button className="google-btn">Регистрация через Google</button>*/}
                        {/*<button className="fb-btn">Регистрация через Facebook</button>*/}
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        signUp: (newUser, userId) => dispatch(signUp(newUser, userId))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PopupJoinPhys);