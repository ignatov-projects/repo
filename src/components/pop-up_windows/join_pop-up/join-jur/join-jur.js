import React from 'react';
import { connect } from "react-redux";
import { signUpCompany } from "../../../actions/authActions";

class PopupJoinJur extends React.Component {

    state = {
        companyName: '',
        email: '',
        phone: '',
        physAddress: '',
        password: '',
        directorName: '',
        authorName: '',
        directorPosition: '',
        authorPosition: '',
        createdProjects: [],
        type: 'company'
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    handleSubmit = (e) => {
        let userId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        let userData = {
            companyName: this.state.companyName,
            email: this.state.email,
            phone: this.state.phone,
            physAddress: this.state.physAddress,
            password: this.state.password,
            directorName: this.state.directorName,
            authorName: this.state.authorName,
            directorPosition: this.state.directorPosition,
            authorPosition: this.state.authorPosition,
            createdProjects: this.state.createdProjects,
            type: this.state.type,
        };

        e.preventDefault();
        this.props.signUpCompany(userData, userId);
        if (this.props.auth.uid){
            window.location.reload()
        }
    };

    render() {

        return (
            <div className="reg-form-content">
                <form className='join-form' onSubmit={this.handleSubmit}>
                    <div className="company-name">
                        <div className="field">
                            <label htmlFor="companyName">Название</label>
                            <input type='text' name='companyName' id='companyName' onChange={this.handleChange} required/>
                        </div>
                    </div>
                    <div className="mail">
                        <div className="field">
                            <label htmlFor="email">E-Mail</label>
                            <input type='text' name='email' id='email' onChange={this.handleChange} required/>
                        </div>
                    </div>
                    <div className="contact">
                        <div className="field">
                            <label htmlFor="phone">Телефон</label>
                            <input type='text' name='phone' id='phone' onChange={this.handleChange} required/>
                        </div>
                        <div className="field">
                            <label htmlFor="physAddress">Физический адрес</label>
                            <input type='text' name='physAddress' id='physAddress' onChange={this.handleChange} required/>
                        </div>
                    </div>
                    <div className="password">
                        <div className="field">
                            <label htmlFor="password">Пароль</label>
                            <input type="password" name='password' id='password' onChange={this.handleChange} required/>
                        </div>
                    </div>
                    <div className="names">
                        <div className="field">
                            <label htmlFor="directorName">ФИО руководителя</label>
                            <input type='text' name='directorName' id='directorName' onChange={this.handleChange} required/>
                        </div>
                        <div className="field">
                            <label htmlFor="authorName">ФИО, кто заполняет форму</label>
                            <input type='text' name='authorName' id='authorName' onChange={this.handleChange} required/>
                        </div>
                    </div>
                    <div className="positions">
                        <div className="field">
                            <label htmlFor="directorPosition">Должность руководителя</label>
                            <input type="text" name='directorPosition' id='directorPosition' onChange={this.handleChange} required/>
                        </div>
                        <div className="field">
                            <label htmlFor="authorPosition">Должность, кто заполняет форму</label>
                            <input type="text" name='authorPosition' id='authorPosition' onChange={this.handleChange} required />
                        </div>
                    </div>
                    <div className="buttons">
                        <button className="primary-btn">Зарегистрироваться</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        signUpCompany: (newUser, userId) => dispatch(signUpCompany(newUser, userId))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PopupJoinJur);