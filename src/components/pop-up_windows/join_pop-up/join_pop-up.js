import React from 'react';
import CloseBtn from "../../../images/close-btn";
import Tabs from "../../tabs_pop-up_windows/tabs";
import PopupJoinPhys from "./join-phys/join-phys";
import PopupJoinJur from "./join-jur/join-jur";

class PopupJoin extends React.Component {

    render() {
        return (
            <section className='reg-form-container'>
                <div className="reg-form-wrapper">
                    <i className='close-btn' onClick={this.props.closePopup}>
                        <CloseBtn/>
                    </i>

                    <h2>Стать благотворителем</h2>

                    <Tabs>
                        <div label="Физ. лицо">
                            <PopupJoinPhys/>
                        </div>
                        <div label="Юр. лицо">
                            <PopupJoinJur togglePopup={this.props.closePopup} />
                        </div>
                    </Tabs>
                </div>
            </section>
        )
    };
}

export default PopupJoin;