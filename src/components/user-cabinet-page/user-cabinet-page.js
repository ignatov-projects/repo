import React from 'react';
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";

class UserCabinet extends React.Component {
    state = {
        projects: ''
    };
    render() {
        if (this.props.user.type === 'person') {
            return (
                <section className='user-cabinet-page-container'>
                    <h1>Кабинет пользователя</h1>
                    <div className="user-cabinet-page-wrapper">
                        <div className="user-cabinet-page-content">
                            <div className="user-information">
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Имя:</h5></span><span
                                    className="information-row"><p>{this.props.user.firstName}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Фамилия:</h5></span><span
                                    className="information-row"><p>{this.props.user.lastName}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Email:</h5></span><span
                                    className="information-row"><p>{this.props.user.email}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Телефон:</h5></span><span
                                    className="information-row"><p>{this.props.user.phone}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Город:</h5></span><span
                                    className="information-row"><p>{this.props.user.city}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Страна:</h5></span><span
                                    className="information-row"><p>{this.props.user.country}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Мои проекты:</h5></span><span className="information-row"><p>Документальный фильм “Скоро”, Осений фестиваль “Мой город”</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Я оказал помощь:</h5></span><span className="information-row"><p>Документальный фильм “Скоро”, Осений фестиваль “Мой город”</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Позиция в рейтинге:</h5></span><span
                                    className="information-row"><p>12 место</p></span>
                                </div>
                            </div>
                            <button className='primary-btn'>Добавить проект</button>
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <section className='user-cabinet-page-container'>
                    <h1>Кабинет пользователя</h1>
                    <div className="user-cabinet-page-wrapper">
                        <div className="user-cabinet-page-content">
                            <div className="user-information">
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Название компании:</h5></span><span
                                    className="information-row"><p>{this.props.user.companyName}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Физический адрес:</h5></span><span
                                    className="information-row"><p>{this.props.user.physAddress}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Email:</h5></span><span
                                    className="information-row"><p>{this.props.user.email}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Телефон:</h5></span><span
                                    className="information-row"><p>{this.props.user.phone}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>ФИО руководителя:</h5></span><span
                                    className="information-row"><p>{this.props.user.directorName}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Должность руководителя:</h5></span><span
                                    className="information-row"><p>{this.props.user.directorPosition}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>ФИО, кто заполняет форму:</h5></span><span
                                    className="information-row"><p>{this.props.user.authorName}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Должность, кто заполняет форму:</h5></span><span
                                    className="information-row"><p>{this.props.user.authorPosition}</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Позиция в рейтинге:</h5></span><span
                                    className="information-row"><p>12</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Мои проекты:</h5></span><span className="information-row"><p>Документальный фильм “Скоро”, Осений фестиваль “Мой город”</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Я оказал помощь:</h5></span><span
                                    className="information-row"><p>Документальный фильм “Скоро”, Осений фестиваль “Мой город”</p></span>
                                </div>
                                <div className="user-cabinet-row">
                                    <span className="name-of-row"><h5>Деньги для сбора:</h5></span><span className="information-row"><p>200 UAH</p></span>
                                </div>
                            </div>
                            <button className='primary-btn'>Добавить проект</button>
                        </div>
                    </div>
                    {console.log(this.props.projects)}
                </section>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const user = state.firebase.profile;
    return {
        user: user,
        projects: state
    }
};

export default compose(
    connect(mapStateToProps)
)(UserCabinet);