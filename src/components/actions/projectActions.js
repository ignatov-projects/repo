export const createProject = (project, projectId, uid) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        fireStore.collection('projects').doc(projectId).set({
            ...project,
            projectDateApplied: new Date()
        }).then(()=>{
            if (uid !== null){
                return (
                    fireStore.collection('users').doc(uid).update({
                        createdProjects: fireStore.FieldValue.arrayUnion(projectId)
                    })
                )
            }
        }).then(() => {
            dispatch({ type: 'CREATE_PROJECT', project });
        }).catch((err) => {
            dispatch({ type: 'CREATE_PROJECT_ERROR', err })
        })
    }
};
export const updateProject = (id) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        fireStore.collection('projects').doc(id).update({
            type: 'active'
        }).then(() => {
            dispatch({ type: 'UPDATE_PROJECT', id });
        }).catch((err) => {
            dispatch({ type: 'UPDATE_PROJECT_ERROR', err })
        })
    }
};
export const archiveProject = (id) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        fireStore.collection('projects').doc(id).update({
            type: 'archive'
        }).then(() => {
            dispatch({ type: 'ARCHIVE_PROJECT', id });
        }).catch((err) => {
            dispatch({ type: 'ARCHIVE_PROJECT_ERROR', err })
        })
    }
};
export const editProject = (id, projectName, projectMoneyGoal, deadline, projectDescription) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        fireStore.collection('projects').doc(id).update({
            projectName: projectName,
            projectMoneyGoal: projectMoneyGoal,
            deadline: deadline,
            projectDescription: projectDescription
        }).then(() => {
            dispatch({ type: 'EDIT_PROJECT', id });
        }).catch((err) => {
            dispatch({ type: 'EDIT_PROJECT_ERROR', err })
        })
    }
};
export const deleteImage = (idx, id) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        const fireBase = getFirebase();
        const storageRef = fireBase.storage().refFromURL(idx);
        storageRef.delete().then(function () {
            console.log(666, 'Delete successful');
        }).catch(function (err) {
            console.log(777, 'delete error', err);
        });
        fireStore.collection('projects').doc(id).update({
            projectImages: fireStore.FieldValue.arrayRemove(idx)
        }).then(() => {
            dispatch({ type: 'DELETE_PROJECT_IMAGE', id });
        }).catch((err) => {
            dispatch({ type: 'DELETE_PROJECT_IMAGE_ERROR', err })
        })
    }
};
export const deleteVideo = (idx, id) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        const fireBase = getFirebase();
        const storageRef = fireBase.storage().refFromURL(idx);
        storageRef.delete().then(function () {
            console.log(666, 'Delete successful');
        }).catch(function (err) {
            console.log(777, 'delete error', err);
        });
        fireStore.collection('projects').doc(id).update({
            projectVideo: null
        }).then(() => {
            dispatch({ type: 'DELETE_PROJECT_VIDEO', id });
        }).catch((err) => {
            dispatch({ type: 'DELETE_PROJECT_VIDEO_ERROR', err })
        })
    }
};
export const declineProject = (id) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        fireStore.collection('projects').doc(id).update({
            type: 'decline'
        }).then(() => {
            dispatch({ type: 'DECLINE_PROJECT', id });
        }).catch((err) => {
            dispatch({ type: 'DECLINE_PROJECT_ERROR', err })
        })
    }
};
export const donate = (donate, donateId, projectId, moneyRaised, donatedMoney, donationsCount) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const fireStore = getFirestore();
        console.log('donate: ', parseInt(donatedMoney, 10));
        fireStore.collection('donations').doc(donateId).set({
            ...donate,
            donateDate: new Date()
        }).then(()=>{
            return (
                fireStore.collection('projects').doc(projectId).update({
                    moneyRaised: +moneyRaised + parseInt(donatedMoney, 10),
                    donationsCount: donationsCount,
                    donations: fireStore.FieldValue.arrayUnion(donateId)
                })
            )
        }).then(() => {
            dispatch({ type: 'PAY_PROJECT', donate });
            window.location.reload();
        }).catch((err) => {
            dispatch({ type: 'PAY_PROJECT_ERROR', err })
        })
    }
};