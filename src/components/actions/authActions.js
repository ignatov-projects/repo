export const signIn = (credentials) => {
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();

        firebase.auth().signInWithEmailAndPassword(
            credentials.email,
            credentials.password
        ).then(() => {
            dispatch({ type: 'LOGIN_SUCCESS' });
        }).catch((err) => {
            dispatch({ type: 'LOGIN_ERROR', err });
        })
    }
};

export const signOut = () => {
    return (dispatch, getState, {getFirebase}) => {
        const firebse = getFirebase();

        firebse.auth().signOut().then(() => {
            dispatch({ type: 'SIGNOUT_SUCCESS' });
        })
    }
};

export const signUp = (newUser, userId) => {
    return (dispatch, getState, {getFirebase, getFirestore}) => {
        const firestore = getFirestore();

        return firestore.collection('users').doc(userId).set({
            ...newUser,
            initials: newUser.firstName[0] + newUser.lastName[0]
        }).then(() => {
            dispatch({ type: 'SIGNUP_SUCCESS' })
        }).catch(err => {
            dispatch({ type: 'SIGNUP_ERROR', err })
        })

    }
};

export const signUpCompany = (newUser) => {
    return (dispatch, getState, {getFirebase, getFirestore}) => {
        const firebase = getFirebase();
        const firestore = getFirestore();

        firebase.auth().createUserWithEmailAndPassword(
            newUser.email,
            newUser.password
        ).then((resp) => {
            return firestore.collection('users').doc(resp.user.uid).set({
                companyName: newUser.companyName,
                directorName: newUser.directorName,
                authorName: newUser.authorName,
                email: newUser.email,
                phone: newUser.phone,
                physAddress: newUser.physAddress,
                directorPosition: newUser.directorPosition,
                authorPosition: newUser.authorPosition,
                type: newUser.type,
                initials: newUser.companyName[0] + newUser.companyName[1]
            }).then(() => {
                dispatch({ type: 'SIGNUP_SUCCESS' })
            }).catch(err => {
                dispatch({ type: 'SIGNUP_ERROR', err })
            })
        })
    }
};