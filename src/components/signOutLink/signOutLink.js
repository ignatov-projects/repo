import React from 'react';
import { connect } from "react-redux";
import { signOut } from "../actions/authActions";
import {Link} from "react-router-dom";
import $ from 'jquery'

const SignedOutLink = (props) => {


    let menu = $('.profile-menu-container');
    $(document).ready(function() {
        $(document).click(function(e) {
            if(!menu.is(e.target) && props.isMenu===true && menu.has(e.target).length === 0){
                props.toggleCloseMenu();
            }
        });
    });



    return (
        <div onClick={props.toggleMenu} className="profile-menu-container">
            <div className='profile-initials'>
                {
                    props.profile.avatar !== null ? <img src={props.profile.avatar} /> :  props.profile.initials
                }

            </div>
            {
                props.isMenu ? (
                <div className="profile-menu-wrapper">
                    <Link to="/cabinet">Мой кабинет</Link>
                    <a  onClick={props.signOut}>Выйти</a>
                </div>) : null
            }

        </div>

    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
};

export default connect(null, mapDispatchToProps)(SignedOutLink)