import React from 'react';
import { connect } from "react-redux";
import { signOut } from "../actions/authActions";
import {Link} from "react-router-dom";

const MobileSignedOutLink = (props) => {

    return (
        <Link to='#' onClick={props.signOut} className='mobile-m-b-item' >Выйти</Link>
    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
};

export default connect(null, mapDispatchToProps)(MobileSignedOutLink)