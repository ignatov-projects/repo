import React from 'react';
import NewsBg from '../../images/bg-news.png';
import EventImg from '../../images/event-image.png';
import NewsTwitter from "../../images/news-twitter";
import NewsFb from "../../images/news-fb";
import OtherNewsBg from '../../images/other-news-bg.png';
import OtherImage1 from '../../images/Rectangle 7.png'

const NewsAboutPage = () => {
    return (
        <section className='news-about-container'>
            <div className="bg-news-img">
                <h1>
                    Мероприятие по професиональному
                    самоопределению
                </h1>
                <div className="opacity"></div>
                <img src={NewsBg} alt=""/>
            </div>
            <div className="event-description-container">
                <h2>Описание мероприятия</h2>
                <div className="event-description">
                    <div className="event-text">
                        <p>
                            Анализ современного состояния детства в украинском обществе показывает, что существует комплекс проблем: скрытый отсев из школ учащихся и превращение их в бродяг, рост хулиганских поступков среди несовершеннолетних подростков, распространение токсикомании и наркомании среди учащейся молодёжи и, как следствие, снижение у них познавательных функций, низкий уровень вербального интеллекта и другие проблемы, которые способствуют стойкому проявлению у подростков социальной неустойчивости и тревожности. Особенно это касается детей-сирот и детей, оставшихся без попечения родителей.
                        </p>
                        <p>В такой ситуации одним из важнейших средств защиты личности является формирование профессионального самоопределения.
                        </p>
                        <p>Необходимость ранней профессиональной подготовки воспитанников детского дома обусловлено тем, что, во-первых, она является одной из эффективных форм подготовки к самостоятельной жизни вне детского дома, во-вторых, она выступает как средство коррекции и реабилитации детей трудом, что очень важно для их личностного и профессионального становления в обществе.</p>
                        <p>Вместе с тем, эффективность современной профориентации оставляет желать лучшего. Опросы школьников и студентов показывают, что профориентационная работа с ними, как правило, либо вовсе не проводилась, либо проводилась формально, не вызывая чувства удовлетворённости. Специалисты-практики, работающие в государственной системе профориентации, жалуются на избыточный контроль и заорганизованность работы. Многие ключевые вопросы, связанные с организацией полноценной системы профориентации, на федеральном уровне всё ещё не решены. Профессиональная ориентация не рассматривается как значимый элемент государственной кадровой политики; нет авторитетного координирующего центра; отсутствуют как система подготовки специалистов по профориентации, так и соответствующий профессиональный стандарт.</p>
                    </div>
                    <div className="event-image">
                        <img src={EventImg} alt=""/>
                        <div className="other-images">
                            <div className="other-images-item">
                                <img src={OtherImage1} alt=""/>
                            </div>
                            <div className="other-images-item">
                                <img src={OtherImage1} alt=""/>
                            </div>
                            <div className="other-images-item">
                                <img src={OtherImage1} alt=""/>
                            </div>
                            <div className="other-images-item">
                                <img src={OtherImage1} alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="share-container">
                    <h3>Поделиться новостью: </h3>
                    <span className="social">
                        <NewsTwitter/>
                        <NewsFb/>
                    </span>
                </div>
            </div>
            <div className="other-news-container">
                <h3>Новости</h3>
                <div className="other-news">
                    <div className="news-card">
                        <img src={OtherNewsBg} alt=""/>
                        <div className="card-news-about">
                            <h5>Мероприятие по професиональному
                                самоопределению.
                            </h5>
                            <p>Сегодня в парке им.Горького прошло
                                мероприятие для детей, где они могли
                                почувствовать себя взрослыми.
                            </p>
                            <button className='desc-news-btn'>
                                Описание
                            </button>
                        </div>
                    </div>
                    <div className="news-card">
                        <img src={OtherNewsBg} alt=""/>
                        <div className="card-news-about">
                            <h5>Мероприятие по професиональному
                                самоопределению.
                            </h5>
                            <p>Сегодня в парке им.Горького прошло
                                мероприятие для детей, где они могли
                                почувствовать себя взрослыми.
                            </p>
                            <button className='desc-news-btn'>
                                Описание
                            </button>
                        </div>
                    </div>
                    <div className="news-card">
                        <img src={OtherNewsBg} alt=""/>
                        <div className="card-news-about">
                            <h5>Мероприятие по професиональному
                                самоопределению.
                            </h5>
                            <p>Сегодня в парке им.Горького прошло
                                мероприятие для детей, где они могли
                                почувствовать себя взрослыми.
                            </p>
                            <button className='desc-news-btn'>
                                Описание
                            </button>
                        </div>
                    </div>
                    <div className="news-card">
                        <img src={OtherNewsBg} alt=""/>
                        <div className="card-news-about">
                            <h5>Мероприятие по професиональному
                                самоопределению.
                            </h5>
                            <p>Сегодня в парке им.Горького прошло
                                мероприятие для детей, где они могли
                                почувствовать себя взрослыми.
                            </p>
                            <button className='desc-news-btn'>
                                Описание
                            </button>
                        </div>
                    </div>
                </div>
                <button className='show-all-btn'>Показать все</button>
            </div>
        </section>
    )
};

export default NewsAboutPage;