import React from 'react';
const NextBtn = () => {
    return (
        <svg width="76" height="78" viewBox="0 0 76 78" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g filter="url(#filter0_d)">
                <ellipse rx="25" ry="25.5381" transform="matrix(-1 0 0 1 36 35.3499)" fill="white"/>
            </g>
            <path d="M32.9536 25.135C33.18 25.1357 33.3981 25.2349 33.5657 25.4133L42.7009 35.0474C42.7951 35.1478 42.8704 35.2701 42.9219 35.4065C42.9734 35.5429 43 35.6904 43 35.8396C43 35.9888 42.9734 36.1363 42.9219 36.2727C42.8704 36.4091 42.7951 36.5314 42.7009 36.6317L33.5657 46.2658C33.4785 46.37 33.374 46.4522 33.2587 46.5073C33.1434 46.5624 33.0197 46.5894 32.8953 46.5865C32.7708 46.5835 32.6481 46.5508 32.5348 46.4903C32.4215 46.4298 32.3199 46.3428 32.2363 46.2347C32.1528 46.1265 32.0889 45.9995 32.0488 45.8614C32.0086 45.7232 31.993 45.577 32.0029 45.4315C32.0127 45.2861 32.0478 45.1445 32.1061 45.0155C32.1643 44.8866 32.2444 44.7729 32.3415 44.6816L40.7277 35.8396L32.3415 26.9976C32.1626 26.8071 32.0554 26.5413 32.0434 26.2584C32.0315 25.9755 32.1157 25.6986 32.2776 25.4883C32.363 25.3774 32.4673 25.2887 32.5837 25.2278C32.7001 25.167 32.8261 25.1354 32.9536 25.135Z" fill="#D64545"/>
            <defs>
                <filter id="filter0_d" x="0" y="0.811768" width="76" height="77.0762" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                    <feOffset dx="2" dy="4"/>
                    <feGaussianBlur stdDeviation="6.5"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
            </defs>
        </svg>

    );
};
export default NextBtn;