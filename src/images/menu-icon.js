import React from 'react';
const MenuIcon = () => {
    return (
        <svg width="31" height="16" viewBox="0 0 31 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M31 0H0V2H31V0Z" fill="white"/>
            <path d="M31 7H5V9H31V7Z" fill="white"/>
            <path d="M31 14H10V16H31V14Z" fill="white"/>
        </svg>
    );
};
export default MenuIcon;