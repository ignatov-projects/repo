import React from 'react';
const PrevBtn = () => {
    return (
        <svg width="76" height="78" viewBox="0 0 76 78" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g filter="url(#filter0_d)">
                <ellipse cx="36" cy="35.3499" rx="25" ry="25.5381" fill="white"/>
            </g>
            <path d="M39.0464 25.135C38.82 25.1357 38.6019 25.2349 38.4343 25.4133L29.2991 35.0474C29.2049 35.1478 29.1296 35.2701 29.0781 35.4065C29.0266 35.5429 29 35.6904 29 35.8396C29 35.9888 29.0266 36.1363 29.0781 36.2727C29.1296 36.4091 29.2049 36.5314 29.2991 36.6317L38.4343 46.2658C38.5215 46.37 38.626 46.4522 38.7413 46.5073C38.8566 46.5624 38.9803 46.5894 39.1047 46.5865C39.2292 46.5835 39.3519 46.5508 39.4652 46.4903C39.5785 46.4298 39.6801 46.3428 39.7637 46.2347C39.8472 46.1265 39.9111 45.9995 39.9512 45.8614C39.9914 45.7232 40.007 45.577 39.9971 45.4315C39.9873 45.2861 39.9522 45.1445 39.8939 45.0155C39.8357 44.8866 39.7556 44.7729 39.6585 44.6816L31.2723 35.8396L39.6585 26.9976C39.8374 26.8071 39.9446 26.5413 39.9566 26.2584C39.9685 25.9755 39.8843 25.6986 39.7224 25.4883C39.637 25.3774 39.5327 25.2887 39.4163 25.2278C39.2999 25.167 39.1739 25.1354 39.0464 25.135Z" fill="#D64545"/>
            <defs>
                <filter id="filter0_d" x="0" y="0.811768" width="76" height="77.0762" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                    <feOffset dx="2" dy="4"/>
                    <feGaussianBlur stdDeviation="6.5"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
            </defs>
        </svg>

    );
};
export default PrevBtn;