const initState = {
    projects: [
        {id: '1', projectName: 'Make a movie', authorFirstName: 'Dmitry', authorLastName: 'Ignatov', authorEmail: 'test@gmail.com', authorPhone: '+380500000000', projectMoneyGoal: '100000', projectMoneyCurrency: 'usd', projectMoneyDonated: '0', projectDeadline: '20.05.2020', projectDescription: 'This is project description.', projectSocialLinks: ['google.com', 'facebook.com'], projectImagesLinks: ['google.com', 'facebook.com'], projectDateApplied: '1.09.2019'},
        {id: '2', projectName: 'Make a jopa', authorFirstName: 'Seryi', authorLastName: 'Pompa', authorEmail: 'fest@gmail.com', authorPhone: '+380500000000', projectMoneyGoal: '500000', projectMoneyCurrency: 'uah', projectMoneyDonated: '0', projectDeadline: '20.05.2020', projectDescription: 'This is project description, mother fuuuuuuuuuu.', projectSocialLinks: ['google.com', 'anus.com'], projectImagesLinks: ['google.com', 'facebook.com'], projectDateApplied: '1.09.2019'}
    ]
};

const projectReducer = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_PROJECT':
            console.log('created project', action.notFinancedProject);
            return state;

        case 'CREATE_PROJECT_ERROR':
            console.log('create project error', action.err);
            return state;

        case 'UPDATE_PROJECT':
            console.log('UPDATED project', action.notFinancedProject);
            return state;

        case 'UPDATE_PROJECT_ERROR':
            console.log('update project error', action.err);
            return state;

        case 'ARCHIVE_PROJECT':
            console.log('UPDATED project', action.archiveProject);
            return state;

        case 'ARCHIVE_PROJECT_ERROR':
            console.log('update project error', action.err);
            return state;

        case 'DECLINE_PROJECT':
            console.log('decline project', action.notFinancedProject);
            return state;

        case 'DECLINE_PROJECT_ERROR':
            console.log('decline project error', action.err);
            return state;

        case 'PAY_PROJECT':
            console.log('pay project', action.toPayProject);
            return state;

        case 'PAY_PROJECT_ERROR':
            console.log('pay project error', action.err);
            return state;

        case 'EDIT_PROJECT':
            console.log('edit project', action.editProject);
            return state;

        case 'EDIT_PROJECT_ERROR':
            console.log('edit project error', action.err);
            return state;

        case 'DELETE_PROJECT_IMAGE':
            console.log('delete project image', action.deleteProjectImage);
            return state;

        case 'DELETE_PROJECT_IMAGE_ERROR':
            console.log('delete project image error', action.err);
            return state;

        case 'DELETE_PROJECT_VIDEO':
            console.log('delete project video', action.deleteProjectVideo);
            return state;

        case 'DELETE_PROJECT_VIDEO_ERROR':
            console.log('delete project video error', action.err);
            return state;

        default:
            return state;
    }
};

export default projectReducer;