import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/auth';

var firebaseConfig = {
    apiKey: "AIzaSyCfBWxwsEJIiqv89fpmzqSHVfh2t_TpDvM",
    authDomain: "redonwhite-33271.firebaseapp.com",
    databaseURL: "https://redonwhite-33271.firebaseio.com",
    projectId: "redonwhite-33271",
    storageBucket: "redonwhite-33271.appspot.com",
    messagingSenderId: "401210685845",
    appId: "1:401210685845:web:861c87d10f596e354c9e9c"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({timestampsInSnapshots: true});

var storage = firebase.storage();


export { storage, firebase as default };